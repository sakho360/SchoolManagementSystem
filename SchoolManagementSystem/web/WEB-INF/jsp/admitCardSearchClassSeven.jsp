<%-- 
    Document   : admitCardSearchClassSeven
    Created on : Oct 1, 2015, 3:09:34 PM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <form action="downloadAdmitCardClassSeven.htm">
                <fieldset>
                    <legend>Download Admit Card Class Nine</legend>
                    <table>
                        <tr>
                            <td>Student's Name</td>
                            <td><input type="text" name="studentName"/></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type="text" name="email"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" value="Download Admit Card"/></td>
                        </tr>
                    </table>
                </fieldset>

            </form>
        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>

