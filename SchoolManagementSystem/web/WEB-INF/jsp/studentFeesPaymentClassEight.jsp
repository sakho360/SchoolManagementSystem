<%-- 
    Document   : studentFeesPaymentClassEight
    Created on : Oct 14, 2015, 10:06:59 AM
    Author     : J2EE 23
--%>

<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSideBarForAdmin.jsp" %>
        </div>
        <div class="col-sm-6">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <form action="classEightPayment.htm">
                <fieldset>
                    <legend>Class Six Payment</legend>
                    <table>
                        <tr>
                            <td>Roll Number</td>
                            <td><input type="text" name="rollNumberEight" value="${Roll}"/></td>
                        </tr>
                        <tr>
                            <td>Student Name</td>
                            <td><input type="text" name="studentName" value="${Student.studentName}"/></td>
                        </tr>
                        <tr>
                            <td>Month</td>
                            <td>
                                <select name="month">
                                    <option value="">Select Month</option>
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td>Amount</td>
                            <td><input type="text" name="amount" id="amountText" onkeyup="countAdvanceDues()"/></td>
                        </tr>
                        <tr>
                            <td>Advance</td>
                            <td><input type="text" name="advance" id="advanceText" value="${List.advance}"/></td>
                        </tr>
                        <tr>
                            <td>Dues</td>
                            <td><input type="text" name="dues" id="duesText" value="${List.dues}"/></td>
                        </tr>
                        <tr>
                            <td><input type="submit" value="Submit"/></td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>

    </div>
</div>

<%@ include file="footerForAdmin.jsp" %>
<script>
    function countAdvanceDues() {
        var amnt = parseFloat(document.getElementById("amountText").value) || 0;
        var adnc = parseFloat(document.getElementById("advanceText").value) || 0;
        var duess = parseFloat(document.getElementById("duesText").value) || 0;
        if (amnt === 0) {
            var advnc2 = parseFloat(adnc + 0);
            var duess2 = parseFloat(duess + 1000);
            document.getElementById("advanceText").value = advnc2;
            document.getElementById("duesText").value = duess2;
        } else if (amnt < 1000) {
            var advnc2 = parseFloat(adnc + 0);
            var duess2 = parseFloat(1000 - amnt);
            document.getElementById("advanceText").value = advnc2;
            document.getElementById("duesText").value = duess2;
        } else if (amnt === 1000) {
            var advnc2 = parseFloat(adnc + 0);
            var duess2 = parseFloat(adnc + 0);
            document.getElementById("advanceText").value = advnc2;
            document.getElementById("duesText").value = duess2;
        }
        else if (amnt > 1000) {
            var amnt2 = parseFloat(document.getElementById("amountText").value);
            var moreAmnt = parseFloat(amnt2 - 1000);
            var advnc2 = parseFloat(moreAmnt + adnc);
            var duess2 = parseFloat(adnc + 0);
            document.getElementById("advanceText").value = advnc2;
            document.getElementById("duesText").value = duess2;
        }
    }
</script>
