<%-- 
    Document   : adminSignupForm
    Created on : Oct 2, 2015, 3:17:06 AM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <form action="signupForAdmin.htm">
                <fieldset>
                    <legend>Admin Signup</legend>
                    <table>
                        <tr>
                            <td>Name</td>
                            <td>&nbsp;</td>
                            <td><input type="text" name="name"/></td>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td>&nbsp;</td>
                            <td><input type="text" name="username"/></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>&nbsp;</td>
                            <td><input type="text" name="email"/></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td>&nbsp;</td>
                            <td><input type="password" name="password"/></td>
                        </tr>
                        <tr>
                            <td>Admin Code</td>
                            <td>&nbsp;</td>
                            <td><input type="password" name="admincode"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>&nbsp;</td>
                            <td><input type="submit" name="Submit"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>&nbsp;</td>
                            <td>${ErrorMessage}</td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>
