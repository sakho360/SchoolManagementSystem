<%-- 
    Document   : findResultClassForClassSix
    Created on : Sep 23, 2015, 1:33:28 AM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6" id="printableArea">
            <table class="table">
                <tr>
                    <td>Name</td>
                    <td>${Student.studentName}</td>
                    <td>Date</td>
                    <td><input type="date" value="${CurrentDate}" readonly="true"></td>
                </tr>
                <tr>
                    <td>ID</td>
                    <td>${Roll}</td>
                </tr>
                <tr>
                    <td>Class</td>
                    <td>Five</td>
                </tr>
            </table>

            <table class="table">
                <th>
                <tr>

                    <td>Subject Name</td>
                    <td>Full Marks</td>
                    <td>Total Obtain Marks</td>
                    <td>Highest Marks</td>
                    <td>Subject Grade</td>
                    <td>Grand Total Marks</td>
                    <td>Obtain Total</td>
                    <td>GPA </td>
                    <td>Grade</td>

                </tr>
                </th>

                <tr>
                    <td>Bangla</td>
                    <td>${Student.banglaMarks}</td>
                    <td>${Student.obtainBangla}</td>
                    <td></td>
                    <td>${Student.banglaGp}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>English</td>
                    <td>${Student.englishMarks}</td>
                    <td>${Student.obtainEnglish}</td>
                    <td></td>
                    <td>${Student.englishGp}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Mathematics</td>
                    <td>${Student.mathematicsMarks}</td>
                    <td>${Student.obtainMathematics}</td>
                    <td></td>
                    <td>${Student.mathematicsGp}</td>
                    <td>${Student.grandTotalMarks}</td>
                    <td>${Student.obtainGrandTotal}</td>
                    <td>${Student.gpa}</td>
                    <td>${Student.grade}</td>
                </tr>
                <tr>
                    <td>General Science</td>
                    <td>${Student.gsMarks}</td>
                    <td>${Student.obtainGs}</td>
                    <td></td>
                    <td>${Student.gsGp}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Social Science</td>
                    <td>${Student.ssMarks}</td>
                    <td>${Student.obtainSs}</td>
                    <td></td>
                    <td>${Student.ssGp}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Religion</td>
                    <td>${Student.religionMarks}</td>
                    <td>${Student.obtainReligion}</td>
                    <td></td>
                    <td>${Student.religionGp}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <input type="button" onclick="printDiv('printableArea')" value="Print!" />
    </div>
</div>

<%@ include file="footer.jsp" %>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>

