<%-- 
    Document   : countingAttendanceClassOne
    Created on : Oct 6, 2015, 7:43:07 PM
    Author     : Maksud Rahaman
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script>
            function comboManipulate() {
                var collectId = ${List};
                var select = document.getElementById("idDropDown");
                for (var i = 0; i < collectId.length; i++) {
                    var option = doccument.createElement('option');
                    option.innerHTML = collectId[i];
                    option.value = collectId[i];
                    select.appendChild(option);
                }
            }

        </script>
    </head>
    <body>
        <form action="insertToAttendanceTableClassOne.htm">


            <table>
                <tr>
                    <td>ID</td>
                    <td>
                        <c:forEach items="${List}" var="list">
                            <select name="rollNumberOne">
                                <option value="${list}">Select</option>
                            </select></td> 
                        </c:forEach>

                </tr>
                <tr>
                    <td>Name</td>
                    <td><input type="text" name="studentName"/></td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td><input type="text" name="date"/></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><input type="radio" name="status"/>Absent</td>
                    <td><input type="radio" name="status"/>Present</td>
                </tr>
                <tr>
                    <td>Count Absent</td>
                    <td><input type="text" name="countAbsent"/></td>
                </tr>
                <tr>
                    <td>Count Present</td>
                    <td><input type="text" name="countPresent"/></td>
                </tr>
                <tr>
                    <td>Class Teacher</td>
                    <td><input type="text" name="classTeacher"/></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" value="save"></td>

                </tr>

            </table>
        </form>
    </body>
</html>
