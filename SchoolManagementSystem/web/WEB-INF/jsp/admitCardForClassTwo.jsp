<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <fieldset>
                <legend>Admit Card For Class Two</legend>
                <table class="table">
                    <tr>
                        <td>Class </td>
                        <td> Two</td>
                        <td>Date</td>
                        <td>${Date}</td>
                    </tr>
                    <tr>
                        <td>Serial Number</td>
                        <td>${Student.serialNumberTwoadtest}</td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td>${Student.studentName}</td>
                        <td></td>
                        <td><img  src="<c:url value="${Student.image}"></c:url>" style=" height: 50px; width: 90px;"/></td>
                    </tr>
                    <tr>
                        <td>Father Name</td>
                        <td>${Student.fatherName}</td>
                    </tr>
                    <tr>
                        <td>Mother Name</td>
                        <td>${Student.motherName}</td>
                    </tr>
                    <tr>
                        <td>Exam Date</td>
                        <td>15-Nov-2015</td>
                    </tr>
                    <tr>
                        <td>Exam Time</td>
                        <td>09.30 AM</td>
                    </tr>
                    <tr>
                        <td>Exam Center</td>
                        <td>Government Laboratory School, Dhanmondi, Dhaka</td>
                    </tr>
                    <tr>
                        <td>Controller</td>
                        <td>Atiqur Rahaman</td>
                    </tr>
                </table>
            </fieldset>

        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>
