<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
        <form action="searchForConfirmAdmissionClassOne.htm">
            <fieldset>
                <legend>Enter Your Serial Number Class One</legend>
                <table>
                    <tr>
                        <td>Enter Your Serial Number</td>
                        <td><input type="text" name="serialNumberOneadtest"/></td>
                    </tr>
                    <tr>
                        <td>Enter Your Email </td>
                        <td><input type="text" name="email"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>${ErrorMessage}</td>
                    </tr>
                </table>
            </fieldset>
        </form>
     </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>

