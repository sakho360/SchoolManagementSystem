<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
<!--        <form action="classOnePayment">
            <fieldset>
                <legend>Class One Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <form action="classTwoPayment">
            <fieldset>
                <legend>Class Two Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <form action="classThreePayment">
            <fieldset>
                <legend>Class Three Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <form action="classFourPayment">
            <fieldset>
                <legend>Class Four Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <form action="classFivePayment">
            <fieldset>
                <legend>Class Five Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>-->
        <form action="classOnePayment">
            <fieldset>
                <legend>Class Six Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name="rollNumberOne" value="${Roll}"/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name="studentName" value="${List.studentName}"/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name="amount"/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name="advance" value="${List.advance}"/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name="dues" value="${List.dues}"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
<!--        <form action="classSevenPayment">
            <fieldset>
                <legend>Class Seven Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <form action="classEightPayment">
            <fieldset>
                <legend>Class Eight Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <form action="classNinePayment">
            <fieldset>
                <legend>Class Nine Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <form action="classTenPayment">
            <fieldset>
                <legend>Class Ten Payment</legend>
                <table>
                    <tr>
                        <td>Roll Number</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Student Name</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Month</td>
                        <td>
                            <select name="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Amount</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Advance</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td>Dues</td>
                        <td><input type="text" name=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"/></td>
                    </tr>
                </table>
            </fieldset>
        </form>-->
        </div>
        
<!--    <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>-->
    </div>
</div>

<%@ include file="footer.jsp" %>
