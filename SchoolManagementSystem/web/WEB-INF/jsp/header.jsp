<%-- 
    Document   : header
    Created on : Sep 10, 2015, 12:40:38 PM
    Author     : J2EE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>School Management System</title>
    </head>
    <body>

        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

        <!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>JSP Page</title>
            <style type="text/css">
                img{
                    width: 100%;
                }
       
         
                /*PopUp*/
                #lean_overlay {
                    position: fixed;
                    z-index: 100;
                    top: 0px;
                    left: 0px;
                    height: 100%;
                    width: 100%;
                    background: #000;
                    display: none;
                }

                .popupContainer {
                    position: absolute;
                    width: 330px;
                    height: auto;
                    left: 45%;
                    top: 60px;
                    background: #FFF;
                }

                #modal_trigger {
                    margin: 5px 10px;
                    width: 100px;
                    display: block;
                    border: 1px solid #DDD;
                    border-radius: 4px;
                    background-color: #B50E0E;
                }
                .modal_trigger {
                    margin: 5px 10px;
                   
                    display: block;
                    border: 1px solid #DDD;
                    border-radius: 4px;
                    background-color: #B50E0E;
                }
                

                .btn_red {
                    background: #ED6347;
                    color: #FFF;
                }

                .btn:hover {
                    background: #E4E4E2;
                }

                .btn_red:hover {
                    background: #C12B05;
                }

                a.btn {
                    color: #666;
                    text-align: center;
                    text-decoration: none;
                }

                a.btn_red {
                    color: #FFF;
                }

                .one_half {
                    width: 50%;
                    display: block;
                    float: left;
                }

                .one_half.last {
                    width: 45%;
                    margin-left: 5%;
                }
                /* Popup Styles*/

                .popupHeader {
                    font-size: 16px;
                    text-transform: uppercase;
                }

                .popupHeader {
                    background: #F4F4F2;
                    position: relative;
                    padding: 10px 20px;
                    border-bottom: 1px solid #DDD;
                    font-weight: bold;
                    border-radius: 10px;
                }

                .popupHeader .modal_close {
                    position: absolute;
                    right: 0;
                    top: 0;
                    padding: 10px 15px;
                    background: #E4E4E2;
                    cursor: pointer;
                    color: #aaa;
                    font-size: 16px;
                    border-radius: 10px;
                }

                .popupBody {
                    padding: 20px;
                }
                /* Social Login Form */

                .social_login {}

                .social_login .social_box {
                    display: block;
                    clear: both;
                    padding: 10px;
                    margin-bottom: 10px;
                    background: #F4F4F2;
                    overflow: hidden;
                }

                .social_login .icon {
                    display: block;
                    width: 10px;
                    padding: 5px 10px;
                    margin-right: 10px;
                    float: left;
                    color: #FFF;
                    font-size: 16px;
                    text-align: center;
                }

                .social_login .fb .icon {
                    background: #3B5998;
                    padding-right: 15px;
                }

                .social_login .google .icon {
                    background: #DD4B39;
                    padding-right: 15px;
                }

                .social_login .icon_title {
                    display: block;
                    padding: 5px 0;
                    float: left;
                    font-weight: bold;
                    font-size: 16px;
                    color: #777;
                }

                .social_login .social_box:hover {
                    background: #E4E4E2;
                }

                .centeredText {
                    text-align: center;
                    margin: 20px 0;
                    clear: both;
                    overflow: hidden;
                    text-transform: uppercase;
                }

                .action_btns {
                    clear: both;
                    overflow: hidden;
                }

                .action_btns a {
                    display: block;
                }
                /* User Login Form */

                .user_login {
                    display: none;
                }

                .user_login label {
                    display: block;
                    margin-bottom: 5px;
                }

                .user_login input[type="text"],
                .user_login input[type="email"],
                .user_login input[type="password"] {
                    display: block;
                    width: 90%;
                    padding: 10px;
                    border: 1px solid #DDD;
                    color: #666;
                }

                .user_login input[type="checkbox"] {
                    float: left;
                    margin-right: 5px;
                }

                .user_login input[type="checkbox"]+label {
                    float: left;
                }

                .user_login .checkbox {
                    margin-bottom: 10px;
                    clear: both;
                    overflow: hidden;
                }

                .forgot_password {
                    display: block;
                    margin: 20px 0 10px;
                    clear: both;
                    overflow: hidden;
                    text-decoration: none;
                    color: #ED6347;
                }
                /* User Register Form */

                .user_register {
                    display: none;
                }

                .user_register label {
                    display: block;
                    margin-bottom: 5px;
                }

                .user_register input[type="text"],
                .user_register input[type="email"],
                .user_register input[type="password"] {
                    display: block;
                    width: 90%;
                    padding: 10px;
                    border: 1px solid #DDD;
                    color: #666;
                }

                .user_register input[type="checkbox"] {
                    float: left;
                    margin-right: 5px;
                }

                .user_register input[type="checkbox"]+label {
                    float: left;
                }

                .user_register .checkbox {
                    margin-bottom: 10px;
                    clear: both;
                    overflow: hidden;
                }
                div#modal {
                    border-radius: 10px;
                }
                /*PopUp*/

            </style>
            <link rel="stylesheet"  href="<c:url value='/images/fonts/font-awesome/css/font-awesome.min.css'/>" type="text/css" media="all">
            <link type="text/css" href="<c:url value='/images/css/bootstrap.min.css'/>" rel="stylesheet"/>
            <link type="text/css" href="<c:url value='/images/css/style.css'/>" rel="stylesheet"/>
            <link type="text/css" href="<c:url value='/images/css/newslider.css'/>" rel="stylesheet"/>
            <script type="text/javascript" src="<c:url value='/images/js/jquery-1.9.1.min.js'/>"></script>
            <script type="text/javascript"  src="<c:url value='/images/js/jssor.js'/>"></script>
            <script type="text/javascript"  src="<c:url value='/images/js/jssor.slider.js'/>"></script>
            <script type="text/javascript" src="<c:url value='/images/js/jquery.js'/>"></script>
            <script type="text/javascript" src="<c:url value='/images/js/jquery(1).js'/>"></script>

        </head>
        <body>


            <div class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <img u="image" src="<c:url value='/images/img/logo_banner_left.jpg'/>" />
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="navbar navbar-default">

                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="navbar-collapse collapse navbar-responsive-collapse">
                                    <form class="navbar-form navbar-right">
                                        <input type="text" class="form-control" placeholder="Search">
                                    </form>
                                    <ul class="nav navbar-nav navbar-left">
                                        <li class="active"><a class="modal_trigger" href="redirectHomePage.htm">Home</a></li>
                                        <li><a id="modal_trigger" href="#modal" class="btn">Login</a></li>
                                        <li><a class="modal_trigger" href="adminSignupPage.htm">Admin Signup </a></li>
                                      
                                        <li><a class="modal_trigger" href="goToSigningInAdmin.htm">Admin Signin</a></li>
                                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Explore<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Contact us</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Further Actions</a></li>
                                            </ul></li>
                                    </ul>
                                </div>
                                <!-- /.nav-collapse -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-2">

                        </div>
                        <div class="col-xs-8 mrq">
                            <h6> <marquee dir="">This is Our School Management System</marquee></h6>
                        </div>
                        <div class="col-xs-2">

                        </div>
                    </div>
                </div>
            </div>

            <div id="modal" class="popupContainer" style="display:none;">
                <header class="popupHeader">
                    <span class="header_title">Login</span>
                    <span class="modal_close"><i class="fa fa-times"></i></span>
                </header>

                <section class="popupBody">
                    <!-- Social Login -->
                    <div class="social_login">
                        <div class="">
                            <a href="#" class="social_box fb">
                                <span class="icon"><i class="fa fa-facebook"></i></span>
                                <span class="icon_title">Connect with Facebook</span>

                            </a>

                            <a href="#" class="social_box google">
                                <span class="icon"><i class="fa fa-google-plus"></i></span>
                                <span class="icon_title">Connect with Google</span>
                            </a>
                        </div>

                        <div class="centeredText">
                            <span>Or use your Email address</span>
                        </div>

                        <div class="action_btns">
                            <div class="one_half"><a href="#" id="login_form" class="btn">Login</a></div>
                            <div class="one_half last"><a href="#" id="register_form" class="btn">Sign up</a></div>
                        </div>
                    </div>

                    <!-- Username & Password Login form -->
                    <div class="user_login">
                        <form action="loginAdminPopup.htm">
                            <label>Email / Username</label>
                            <input type="email" required="" name="email">
                            <br>

                            <label>Password</label>
                            <input type="password" required="" name="password">
                            <br>
                            <label>Admin Code</label>
                            <input type="password" required="" name="admincode">
                            <br>

                            <div class="checkbox">
                                <input id="remember" type="checkbox">
                                <label for="remember">Remember me on this computer</label>
                            </div>

                            <div class="action_btns">
                                <div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i> Back</a></div>
                                <div class="one_half last"><input type="submit" value="Login"/>
                                
                                </div>
                            </div>
                        </form>

                        <a href="#" class="forgot_password">Forgot password?</a>
                    </div>

                    <!-- Register Form -->
                    <div class="user_register">
                        <form action="registerAdmin.htm">
                            <label>Full Name</label>
                            <input type="text" name="name" required="">
                            <br>
                            <label>User Name</label>
                            <input type="text" name="username" required="">
                            <br>

                            <label>Email Address</label>
                            <input type="email" name="email" required="">
                            <br>

                            <label>Password</label>
                            <input type="password" name="password" required="">
                            <br>
                            <label>Admin Code</label>
                            <input type="password" name="admincode" required="">
                            <br>

                            <div class="checkbox">
                                <input id="send_updates" type="checkbox">
                                <label for="send_updates">Send me occasional email updates</label>
                            </div>

                            <div class="action_btns">
                                <div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i> Back</a></div>
                                <div class="one_half last">
                                    <input type="submit" id="registerid" value="Register" class="btn btn_red"/></div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>