<%-- 
    Document   : admissionTestPhotoClassSeven
    Created on : Oct 13, 2015, 4:43:20 PM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <form action="admissionTestPhotoClassSeven.htm" enctype="multipart/form-data" method="POST">
                <table>
                    <tr>
                        <td>Choose Your Image</td>
                    </tr>
                    <tr>
                        <td><input type="file" name="image"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Upload"/></td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>
