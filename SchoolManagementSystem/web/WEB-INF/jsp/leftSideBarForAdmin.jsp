<%-- 
    Document   : leftSideBarForAdmin
    Created on : Oct 11, 2015, 9:39:14 PM
    Author     : Maksud Rahaman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <nav>
            <div class="menu-item alpha">
                <h4><a href="#">Students Information and Manipulation &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <li><a href="goToShowStudentInformationClassOne.htm">Class One</a></li>
                    <li><a href="goToShowStudentInformationClassTwo.htm">Class Two</a></li>
                    <li><a href="goToShowStudentInformationClassThree.htm">Class Three</a></li>
                    <li><a href="goToShowStudentInformationClassFour.htm">Class Four</a></li>
                    <li><a href="goToShowStudentInformationClassFive.htm">Class Five</a></li>
                    <li><a href="goToShowStudentInformationClassSix.htm">Class Six</a></li>
                    <li><a href="goToShowStudentInformationClassSeven.htm">Class Seven</a></li>
                    <li><a href="goToShowStudentInformationClassEight.htm">Class Eight</a></li>
                    <li><a href="goToShowStudentInformationClassNine.htm">Class Nine</a></li>
                    <li><a href="goToShowStudentInformationClassTen.htm">Class Ten</a></li>


                </ul>
            </div>

            <div class="menu-item alpha">
                <h4><a href="#"> Create Result For All Classes &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <li><a href="goToCreateResultClassOne.htm">Class One</a></li>
                    <li><a href="goToCreateResultClassTwo.htm">Class Two</a></li>
                    <li><a href="goToCreateResultClassThree.htm">Class Three</a></li>
                    <li><a href="goToCreateResultClassFour.htm">Class Four</a></li>
                    <li><a href="goToCreateResultClassFive.htm">Class Five</a></li>
                    <li><a href="goToCreateResultClassSix.htm">Class Six</a></li>
                    <li><a href="goToCreateResultClassSeven.htm">Class Seven</a></li>
                    <li><a href="goToCreateResultClassEight.htm">Class Eight</a></li>
                    <li><a href="goToCreateResultClassNine.htm">Class Nine</a></li>
                    <li><a href="goToCreateResultClassTen.htm">Class Ten</a></li>



                </ul>

            </div>

            <div class="menu-item alpha">
                <h4><a href="#">Students' Fees Payment Status  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>

                    <li><a href="goToSearchFeesPaymentStatusClassOne.htm">Class One</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassTwo.htm">Class Two</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassThree.htm">Class Three</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassFour.htm">Class Four</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassFive.htm">Class Five</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassSix.htm">Class Six</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassSeven.htm">Class Seven</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassEight.htm">Class Eight</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassNine.htm">Class Nine</a></li>
                    <li><a href="goToSearchFeesPaymentStatusClassTen.htm">Class Ten</a></li>
                </ul>

            </div>

            <div class="menu-item alpha ">
                <h4><a href="#">Upload Notices  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <li><a href="#">Notice For Teacher</a></li>
                    <li><a href="#">Notice For Student</a></li>
                    <!--                    <li><a href="goToConfirmAdmissionClassThree.htm">Class Three</a></li>
                                        <li><a href="goToConfirmAdmissionClassFour.htm">Class Four</a></li>
                                        <li><a href="goToConfirmAdmissionClassFive.htm">Class Five</a></li>
                                        <li><a href="goToConfirmAdmissionClassSix.htm">Class Six</a></li>
                                        <li><a href="goToConfirmAdmissionClassSeven.htm">Class Seven</a></li>
                                        <li><a href="goToConfirmAdmissionClassEight.htm">Class Eight</a></li>
                                        <li><a href="goToConfirmAdmissionClassNine.htm">Class Nine</a></li>-->

                </ul>

            </div>
            <div class="menu-item alpha ">
                <h4><a href="#">Teachers' Information   &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <ul>
                        <li><a href="goToAddNewTeacher.htm">Add New Teacher  </a></li>
                        <li><a href="#">Modify Teachers' Information </a></li>
                        <li><a href="#">Available Teachers</a></li>
                        <li><a href="#">Erase Teacher's Information  </a></li>

                    </ul>

                </ul>

            </div>
            <div class="menu-item alpha ">
                <h4><a href="#">No Caption &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <!--                <ul>
                                    <li><a href="goToDownloadAdmitCardOne.htm">Class One</a></li>
                                    <li><a href="goToDownloadAdmitCardTwo.htm">Class Two</a></li>
                                    <li><a href="goToDownloadAdmitCardThree.htm">Class Three</a></li>
                                    <li><a href="goToDownloadAdmitCardFour.htm">Class Four</a></li>
                                    <li><a href="goToDownloadAdmitCardFive.htm">Class Five</a></li>
                                    <li><a href="goToDownloadAdmitCardSix.htm">Class Six</a></li>
                                    <li><a href="goToDownloadAdmitCardSeven.htm">Class Seven</a></li>
                                    <li><a href="goToDownloadAdmitCardEight.htm">Class Eight</a></li>
                                    <li><a href="goToDownloadAdmitCardNine.htm">Class Nine</a></li>
                                </ul>-->
            </div>

            <div class="menu-item alpha">
                <h4><a href="#">No Caption &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <!--                    <li><a href="goToAddNewTeacher.htm">Add New Teacher  </a></li>
                                        <li><a href="#">Modify Teachers' Information </a></li>
                                        <li><a href="#">Available Teachers</a></li>
                                        <li><a href="#">Erase Teacher's Information  </a></li>-->

                </ul>

            </div>

            <div class="menu-item alpha">
                <h4><a href="#">No Caption  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <!--                    <li><a href="goToSearchStatusClassOne.htm">Class One</a></li>
                                        <li><a href="goToSearchStatusClassTwo.htm">Class Two</a></li>
                                        <li><a href="goToSearchStatusClassThree.htm">Class Three</a></li>
                                        <li><a href="goToSearchStatusClassFour.htm">Class Four</a></li>
                                        <li><a href="goToSearchStatusClassFive.htm">Class Five</a></li>
                                        <li><a href="goToSearchStatusClassSix.htm">Class Six</a></li>
                                        <li><a href="goToSearchStatusClassSeven.htm">Class Seven</a></li>
                                        <li><a href="goToSearchStatusClassEight.htm">Class Eight</a></li>
                                        <li><a href="goToSearchStatusClassNine.htm">Class Nine</a></li>
                                        <li><a href="goToSearchStatusClassTen.htm">Class Ten</a></li>-->

                </ul>

            </div>

            <div class="menu-item alpha ">
                <h4><a href="#">No Caption  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <!--                    <li><a href="goToShowInfoToPayClassOneFees.htm">Class One</a></li>
                                        <li><a href="goToShowInfoToPayClassTwoFees.htm">Class Two</a></li>
                                        <li><a href="goToShowInfoToPayClassThreeFees.htm">Class Three</a></li>
                                        <li><a href="goToShowInfoToPayClassFourFees.htm">Class Four</a></li>
                                        <li><a href="goToShowInfoToPayClassFiveFees.htm">Class Five</a></li>
                                        <li><a href="goToShowInfoToPayClassSixFees.htm">Class Six</a></li>
                                        <li><a href="goToShowInfoToPayClassSevenFees.htm">Class Seven</a></li>
                                        <li><a href="goToShowInfoToPayClassEightFees.htm">Class Eight</a></li>
                                        <li><a href="goToShowInfoToPayClassNineFees.htm">Class Nine</a></li>
                                        <li><a href="goToShowInfoToPayClassTenFees.htm">Class Ten</a></li>-->
                </ul>

            </div>
            <div class="menu-item alpha ">
                <h4><a href="#">No Caption  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                <ul>
                    <!--                    <li><a href="goToCreateAttendanceForClassOne.htm">Class One</a></li>
                                        <li><a href="goToCreateAttendanceForClassTwo.htm">Class Two</a></li>
                                        <li><a href="goToCreateAttendanceForClassThree.htm">Class Three</a></li>
                                        <li><a href="goToCreateAttendanceForClassFour.htm">Class Four</a></li>
                                        <li><a href="goToCreateAttendanceForClassFive.htm">Class Five</a></li>
                                        <li><a href="goToCreateAttendanceForClassSix.htm">Class Six</a></li>
                                        <li><a href="goToCreateAttendanceForClassSeven.htm">Class Seven</a></li>
                                        <li><a href="goToCreateAttendanceForClassEight.htm">Class Eight</a></li>
                                        <li><a href="goToCreateAttendanceForClassNine.htm">Class Nine</a></li>
                                        <li><a href="goToCreateAttendanceForClassTen.htm">Class Ten</a></li>-->

                </ul>

            </div>
        </nav>
    </body>
</html>

