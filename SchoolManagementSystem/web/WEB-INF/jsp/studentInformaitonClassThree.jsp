<%-- 
    Document   : studentInformaitonClassThree
    Created on : Oct 13, 2015, 11:36:30 PM
    Author     : Maksud Rahaman
--%>

<%@ include file="headerForAdmin.jsp" %>
<div class="container">
    <div class="row">
        <!--        <div class="col-sm-3 subDropdown">
                   
                </div>-->
        <div class="col-sm-12">
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <fieldset>
                <legend>Class Three Student Information</legend>
                <table class="table">
                    <th>
                    <tr>
                        <td>Student's Identity</td>
                        
                        <td>Student's Name</td>
                        
                        <td>Father's Name</td>
                        
                        <td>Mother's Name</td>
                       
                        <td>Date Of Birth</td>
                        
                        <td>Student's Address</td>
                        
                        <td>Religion</td>
                        
                        <td>Gender</td>
                        
                        <td>Student's Email</td>
                       
                        <td>Student's Phone</td>
                        
                        <td>Nationality</td>
                        
                        <td>Image</td>
                        <td>&nbsp;</td>

                        <td>&nbsp;</td>
                        

                        <td>&nbsp;</td>
                        

                        <td>&nbsp;</td>
                        

                        <td>&nbsp;</td>
                        

                    </tr>
                    </th>
                    <c:forEach var="list" items="${List}">
                        <tr>
                            <td>${list.rollNumberThree}</td>
                            
                            <td>${list.studentName}</td>
                            
                            <td>${list.fatherName}</td>
                            
                            <td>${list.motherName}</td>
                            
                            <td>${list.dateOfBirth}</td>
                            
                            <td>${list.presentAddress}</td>
                            
                            <td>${list.religion}</td>
                            
                            <td>${list.gender}</td>
                            
                            <td>${list.email}</td>
                            
                            <td>${list.phone}</td>
                            
                            <td>${list.nationality}</td>
                            
                            <td><img  src="<c:url value="${list.image}"></c:url>" style=" height: 80px; width: 50px;"/></td>
                                
                                <td><a href="goToPayingFeesClassThree.htm?rollNumberThree=${list.rollNumberThree}&studentName=${list.studentName}"> Fees</a></td>
                            
                            <td><a href="goToCreateAttendanceClassThree.htm?rollNumberThree=${list.rollNumberThree}&studentName=${list.studentName}"> Attendance</a></td>
                            
                            <td><a href="goToEditClassThree.htm?rollNumberThree=${list.rollNumberThree}"> Edit</a></td>
                            
                            <td><a href="doDeleteClassThree.htm?rollNumberThree=${list.rollNumberThree}"> Delete</a></td>
                            
                        </tr>
                    </c:forEach>
                </table>
            </fieldset>
        </div>

    </div>
</div>

<%@ include file="footerForAdmin.jsp" %>
