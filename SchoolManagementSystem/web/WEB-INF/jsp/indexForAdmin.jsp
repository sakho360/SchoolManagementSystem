<%-- 
    Document   : indexForAdmin
    Created on : Oct 11, 2015, 9:41:54 PM
    Author     : Maksud Rahaman
--%>

<%@ include file="headerForAdmin.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 subDropdown">
                    <%@ include file="leftSideBarForAdmin.jsp" %>
                </div>
                <div class="col-sm-6">
                    <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 100%;
                         height: 456px; background: #191919; overflow: hidden;">

                        <!-- Loading Screen -->
                        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                                 background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
                            </div>
                            <div style="position: absolute; display: block; background: url(images/img/loading.gif) no-repeat center center;
                                 top: 0px; left: 0px;width: 100%;height:100%;">
                            </div>
                        </div>

                        <!-- Slides Container -->
                        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1500px;  height: 500px; overflow: hidden;">
                            <div>

                                <img u="image" src="<c:url value='/images/img/alila/01.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-01.jpg'/>" />
                            </div>
                            <div>
                                <img src="<c:url value='/images/img/alila/02.jpg'/>" />

                                <img src="<c:url value='/images/img/alila/thumb-02.jpg'/>" />

                            </div>
                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/03.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-03.jpg'/>" />

                            </div>
                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/04.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-04.jpg'/>" />
                            </div>
                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/05.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-05.jpg'/>" />
                            </div>
                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/06.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-06.jpg'/>" />
                            </div>
                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/07.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-07.jpg'/>" />
                            </div>
                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/08.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-08.jpg'/>" />
                            </div>
                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/09.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-09.jpg'/>" />
                            </div>
                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/11.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-11.jpg'/>" />
                            </div>

                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/12.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-12.jpg'/>" />
                            </div>
                            <div>
                                <img u="image" src="<c:url value='/images/img/alila/13.jpg'/>" />

                                <img u="thumb" src="<c:url value='/images/img/alila/thumb-13.jpg'/>" />
                            </div>
                        </div>

                        <span u="arrowleft" class="jssora05l" style="top: 158px; left: 8px;">
                        </span>
                        <!-- Arrow Right -->
                        <span u="arrowright" class="jssora05r" style="top: 158px; right: 8px">
                        </span>



                        <div u="thumbnavigator" class="jssort01" style="left: 0px; bottom: 0px;">
                            <!-- Thumbnail Item Skin Begin -->
                            <div u="slides" style="cursor: default;">
                                <div u="prototype" class="p">
                                    <div class=w><div u="thumbnailtemplate" class="t"></div></div>
                                    <div class=c></div>
                                </div>
                            </div>
                            <!-- Thumbnail Item Skin End -->
                        </div>
                    </div>
                    <div class="para">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type
                            specimen book. It has survived not only five centuries, but also the leap into
                            electronic typesetting, remaining essentially unchanged. It was popularised in
                            the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                            and more recently with desktop publishing software like Aldus PageMaker
                            including versions of Lorem Ipsum.</p>
                        <p>
                            Contrary to popular belief, Lorem Ipsum is not simply random text.
                            It has roots in a piece of classical Latin literature from 45 BC, 
                            making it over 2000 years old. Richard McClintock, a Latin professor 
                            at Hampden-Sydney College in Virginia, looked up one of the more
                            obscure Latin words, consectetur, from a Lorem Ipsum passage, 
                            and going through the cites of the word in classical literature, 
                            discovered the undoubtable source. Lorem Ipsum comes from sections 
                            1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil)
                            by Cicero, written in 45 BC. This book is a treatise on 
                            the theory of ethics, very popular during the Renaissance. 
                            The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet.
                            .", comes from a line in section 1.10.32.</p>
                    </div>
                </div>
                <div class="col-sm-3 subDropdown">
                    <nav>
                        <div class="menu-item ">
                            <h4><a href="#">Lorem ipsum dolor&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>


                            </ul>
                        </div>

                        <div class="menu-item alpha">
                            <h4><a href="#">nostrud exercita &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>
                            </ul>

                        </div>

                        <div class="menu-item alpha">
                            <h4><a href="#">ullamco laboris  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>

                            </ul>

                        </div>

                        <div class="menu-item alpha ">
                            <h4><a href="#">nisi ut aliquip ex  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>

                            </ul>

                        </div>
                        <div class="menu-item alpha ">
                            <h4><a href="#">ea commodo  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>

                            </ul>

                        </div>
                        <div class="menu-item ">
                            <h4><a href="#">Lorem ipsum dolor&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>


                            </ul>
                        </div>

                        <div class="menu-item alpha">
                            <h4><a href="#">nostrud exercita &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>
                            </ul>

                        </div>

                        <div class="menu-item alpha">
                            <h4><a href="#">ullamco laboris  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>

                            </ul>

                        </div>

                        <div class="menu-item alpha ">
                            <h4><a href="#">nisi ut aliquip ex  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>

                            </ul>

                        </div>
                        <div class="menu-item alpha ">
                            <h4><a href="#">ea commodo  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<i class="fa fa-angle-double-down"></i></a></h4>
                            <ul>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">incididunt ut labore  </a></li>
                                <li><a href="#">amet consectetur  </a></li>
                                <li><a href="#">adipiscing elit sed do</a></li>
                                <li><a href="#">eiusmod tempor   </a></li>
                                <li><a href="#">  </a></li>

                            </ul>

                        </div>
                    </nav> 
                </div>
            </div>
        </div>
       <%@ include file="footerForAdmin.jsp" %>