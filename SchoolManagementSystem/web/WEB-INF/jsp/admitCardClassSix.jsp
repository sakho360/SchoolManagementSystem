<%-- 
    Document   : admitCardClassSix
    Created on : Sep 13, 2015, 11:56:24 AM
    Author     : Maksud Rahaman
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <c:forEach items="${ListSix}" var="list">
                <tr>
                <td>Serial Number</td>
                <td>${list.serialNumber}</td>
            </tr>
            <tr>
                <td>Student Name</td>
                <td>${list.studentName}</td>
            </tr>
            <tr>
                <td>Father Name</td>
                <td>${list.fatherName}</td>
            </tr>
            <tr>
                <td>Mother Name</td>
                <td>${list.motherName}</td>
            </tr>
            </c:forEach>
            
            <tr>
                <td>Exam Date</td>
                <td>2015-10-10</td>
            </tr>
            <tr>
                <td>Exam Center</td>
                <td>Dhaka COllege</td>
            </tr>
            <tr>
                <td>Exam Time</td>
                <td>09.00 am</td>
            </tr>
            <tr>
                <td>Image</td>
                <td></td>
            </tr>
            <tr>
                <td>Exam Controller</td>
                <td></td>
            </tr>
        </table>
    </body>
</html>
