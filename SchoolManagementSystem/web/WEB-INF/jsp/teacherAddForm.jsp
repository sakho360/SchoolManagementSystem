<%-- 
    Document   : teacherAddForm
    Created on : Oct 2, 2015, 9:44:30 PM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
    <body>
        <form action="addNewTeacher.htm">
            <fieldset>

                <legend>Add New Teacher</legend>
                <table>
                    <tr>
                        <td>Teacher ID</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="teacherId"/></td>
                    </tr>
                    <tr>
                        <td>Teacher Name</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="teacherName"/></td>
                    </tr>
                    <tr>
                        <td>Group</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name="group">
                                <option>Select Group</option>
                                <option value="Business Studies">Business Studies</option>
                                <option value="Humanities">Humanities</option>
                                <option value="Science">Science</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Group</td>
                        <td>&nbsp;</td>
                        <td>
                            <select name="designation">
                                <option>Select Designation</option>
                                <option value="Principal">Principal</option>
                                <option value="Assistant Principal">Assistant Principal</option>
                                <option value="Senior Assistant Teacher">Senior Assistant Teacher</option>
                                <option value="Assistant Teacher">Assistant Teacher</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Permanent Address</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="permanentAddress"/></td>
                    </tr>
                    <tr>
                        <td>Present Address</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="presentAddress"/></td>
                    </tr>
                    <tr>
                        <td>Join Date</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="joinDate"/></td>
                    </tr>
                    <tr>
                        <td>Date Of Birth</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="dateOfBirth"/></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="email" /></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="phone"/></td>
                    </tr>
                    <tr>
                        <td>Nationality</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="nationnality"/></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="radio" name="gender" value="Male"/>Male &nbsp;
                            <input type="radio" name="gender" value="Female"/>Female &nbsp;
                            <input type="radio" name="gender" value="Third"/>Third &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>Educational Qualification</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="educationalQualification"/></td>
                    </tr>
                    <tr>
                        <td>Image</td>
                        <td>&nbsp;</td>
                        <td><input type="text" name="image"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td><input type="submit" value="Submit" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>${Message}</td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>
