<%-- 
    Document   : findResultForClassSix
    Created on : Sep 23, 2015, 1:04:00 AM
    Author     : Maksud Rahaman
--%>

<%@ include file="header.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
        <div class="col-sm-6">
            <fieldset>
                <legend>Search Class Two Result</legend>
                <form action="findResultForClassTwo.htm">
                    <table>
                        <tr>
                            <td>
                                Roll Number
                            </td>
                            <td>
                                <input type="text" name="rollNumberTwo"/>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>
                                <input type="submit" value="Submit"/>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>
                                ${ErrorMessage}
                            </td>
                        </tr>
                    </table>


                </form>
            </fieldset>

        </div>
        <div class="col-sm-3 subDropdown">
            <%@ include file="leftSidebar.jsp" %>
        </div>
    </div>
</div>

<%@ include file="footer.jsp" %>
