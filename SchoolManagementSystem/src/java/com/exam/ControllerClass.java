/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exam;

import com.entity.AdminLoginTable;
import com.entity.ClassEightAdmissionTable;
import com.entity.ClassEightAdmissionTestTable;
import com.entity.ClassEightAttendanceTable;
import com.entity.ClassEightPaymentTable;
import com.entity.ClassEightResultTable;
import com.entity.ClassFiveAdmissionTable;
import com.entity.ClassFiveAdmissionTestTable;
import com.entity.ClassFiveAttendanceTable;
import com.entity.ClassFivePaymentTable;
import com.entity.ClassFiveResultTable;
import com.entity.ClassFourAdmissionTable;
import com.entity.ClassFourAdmissionTestTable;
import com.entity.ClassFourAttendanceTable;
import com.entity.ClassFourPaymentTable;
import com.entity.ClassFourResultTable;
import com.entity.ClassNineAdmissionTable;
import com.entity.ClassNineAdmissionTestTable;
import com.entity.ClassNineAttendanceTable;
import com.entity.ClassNinePaymentTable;
import com.entity.ClassNineResultTable;
import com.entity.ClassOneAdmissionTable;
import com.entity.ClassOneAdmissionTestTable;
import com.entity.ClassOneAttendanceTable;
import com.entity.ClassOnePaymentTable;
import com.entity.ClassOneResultTable;
import com.entity.ClassSevenAdmissionTable;
import com.entity.ClassSevenAdmissionTestTable;
import com.entity.ClassSevenAttendanceTable;
import com.entity.ClassSevenPaymentTable;
import com.entity.ClassSevenResultTable;
import com.entity.ClassSixAdmissionTable;
import com.entity.ClassSixAdmissionTestTable;
import com.entity.ClassSixAttendanceTable;
import com.entity.ClassSixPaymentTable;
import com.entity.ClassSixResultTable;
import com.entity.ClassTenAttendanceTable;
import com.entity.ClassTenPaymentTable;
import com.entity.ClassTenResultTable;
import com.entity.ClassTenStudentInformationTable;
import com.entity.ClassThreeAdmissionTable;
import com.entity.ClassThreeAdmissionTestTable;
import com.entity.ClassThreeAttendanceTable;
import com.entity.ClassThreePaymentTable;
import com.entity.ClassThreeResultTable;
import com.entity.ClassTwoAdmissionTable;
import com.entity.ClassTwoAdmissionTestTable;
import com.entity.ClassTwoAttendanceTable;
import com.entity.ClassTwoPaymentTable;
import com.entity.ClassTwoResultTable;
import com.entity.StuffInformationTable;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Maksud Rahaman
 */
@Controller
public class ControllerClass {

    @RequestMapping("/redirectHomePage")
    public String redirectHomePage() {
        return "index";
    }

    @RequestMapping("/adminPage")
    public String redirectadminPage() {
        return "indexForAdmin";
    }

    //Start of Flow controll to show admission test pages
    @RequestMapping("/goToClassOneTest")
    public String goToClassOneTest() {
        return "admissionTestPhotoClassOne";
    }

    @RequestMapping("/goToClassTwoTest")
    public String goToClassTwoTest() {
        return "admissionTestPhotoClassTwo";
    }

    @RequestMapping("/goToClassThreeTest")
    public String goToClassThreeTest() {
        return "admissionTestPhotoClassThree";
    }

    @RequestMapping("/goToClassFourTest")
    public String goToClassFourTest() {
        return "admissionTestPhotoClassFour";
    }

    @RequestMapping("/goToClassFiveTest")
    public String goToClassFiveTest() {
        return "admissionTestPhotoClassFive";
    }

    @RequestMapping("/goToClassSixTest")
    public String goToClassSixTest() {
        return "admissionTestPhotoClassSix";
    }

    @RequestMapping("/goToClassSevenTest")
    public String goToClassSevenTest() {
        return "admissionTestPhotoClassSeven";
    }

    @RequestMapping("/goToClassEightTest")
    public String goToClassEightTest() {
        return "admissionTestPhotoClassEight";
    }

    @RequestMapping("/goToClassNineTest")
    public String goToClassNineTest() {
        return "admissionTestPhotoClassNine";
    }
    //End of Flow controll to show admission test pages

    //Start Flow to go to ConfirmAdmission
    @RequestMapping("/goToConfirmAdmissionClassOne")
    public String goToConfirmAdmissionClassOne() {
        return "confirmAdmissionForClassOne";
    }

    @RequestMapping("/goToConfirmAdmissionClassTwo")
    public String goToConfirmAdmissionClassTwo() {
        return "confirmAdmissionForClassTwo";
    }

    @RequestMapping("/goToConfirmAdmissionClassThree")
    public String goToConfirmAdmissionClassThree() {
        return "confirmAdmissionForClassThree";
    }

    @RequestMapping("/goToConfirmAdmissionClassFour")
    public String goToConfirmAdmissionClassFour() {
        return "confirmAdmissionForClassFour";
    }

    @RequestMapping("/goToConfirmAdmissionClassFive")
    public String goToConfirmAdmissionClassFive() {
        return "confirmAdmissionForClassFive";
    }

    @RequestMapping("/goToConfirmAdmissionClassSix")
    public String goToConfirmAdmissionClassSix() {
        return "confirmAdmissionForClassSix";
    }

    @RequestMapping("/goToConfirmAdmissionClassSeven")
    public String goToConfirmAdmissionClassSeven() {
        return "confirmAdmissionForClassSeven";
    }

    @RequestMapping("/goToConfirmAdmissionClassEight")
    public String goToConfirmAdmissionClassEight() {
        return "confirmAdmissionForClassEight";
    }

    @RequestMapping("/goToConfirmAdmissionClassNine")
    public String goToConfirmAdmissionClassNine() {
        return "confirmAdmissionForClassNine";
    }
    // End Of Flow to go to ConfirmAdmission

    //Start of Flow to ShowInformationToPayFees
    @RequestMapping("/goToShowInfoToPayClassOneFees")
    public String goToShowInfoToPayClassOneFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassOneAdmissionTable> list = modelBean.findAllInformationForFeesPaymentClassOne();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassOne";
    }

    @RequestMapping("/goToShowInfoToPayClassTwoFees")
    public String goToShowInfoToPayClassTwoFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassTwoAdmissionTable> list = modelBean.findAllInformationForFeesPaymentClassTwo();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassTwo";
    }

    @RequestMapping("/goToShowInfoToPayClassThreeFees")
    public String goToShowInfoToPayClassThreeFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassThreeAdmissionTable> list = modelBean.findAllInformationForFeesPaymentClassThree();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassThree";
    }

    @RequestMapping("/goToShowInfoToPayClassFourFees")
    public String goToShowInfoToPayClassFourFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassFourAdmissionTable> list = modelBean.findAllInformationForFeesPaymentClassFour();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassFour";
    }

    @RequestMapping("/goToShowInfoToPayClassFiveFees")
    public String goToShowInfoToPayClassFiveFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassFiveAdmissionTable> list = modelBean.findAllInformationForFeesPaymentClassFive();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassFive";
    }

    @RequestMapping("/goToShowInfoToPayClassSixFees")
    public String goToShowInfoToPayClassSixFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassSixAdmissionTable> list = modelBean.findAllInformationForFeesPaymentClassSix();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassSix";
    }

    @RequestMapping("/goToShowInfoToPayClassSevenFees")
    public String goToShowInfoToPayClassSevenFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassSevenAdmissionTable> list = modelBean.findAllInformationForFeesPaymentClassSeven();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassSeven";
    }

    @RequestMapping("/goToShowInfoToPayClassEightFees")
    public String goToShowInfoToPayClassEightFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassEightAdmissionTable> list = modelBean.findAllInformationForFeesPaymentClassEight();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassEight";
    }

    @RequestMapping("/goToShowInfoToPayClassNineFees")
    public String goToShowInfoToPayClassNineFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassNineAdmissionTable> list = modelBean.findAllInformationForFeesPaymentClassNine();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassNine";
    }

    @RequestMapping("/goToShowInfoToPayClassTenFees")
    public String goToShowInfoToPayClassTenFees(Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassTenStudentInformationTable> list = modelBean.findAllInformationForFeesPaymentClassTen();
        m.addAttribute("List", list);
        return "showInformationForPayingFeesClassTen";
    }

    //Start Flow controll for going to CreateResult Page
    @RequestMapping("/goToCreateResultClassOne")
    public String goToCreateResultClassOne() {
        return "createResultForClassOne";
    }

    @RequestMapping("/goToCreateResultClassTwo")
    public String goToCreateResultClassTwo() {
        return "createResultForClassTwo";
    }

    @RequestMapping("/goToCreateResultClassThree")
    public String goToCreateResultClassThree() {
        return "createResultForClassThree";
    }

    @RequestMapping("/goToCreateResultClassFour")
    public String goToCreateResultClassFour() {
        return "createResultForClassFour";
    }

    @RequestMapping("/goToCreateResultClassFive")
    public String goToCreateResultClassFive() {
        return "createResultForClassFive";
    }

    @RequestMapping("/goToCreateResultClassSix")
    public String goToCreateResultClassSix() {
        return "createResultForClassSix";
    }

    @RequestMapping("/goToCreateResultClassSeven")
    public String goToCreateResultClassSeven() {
        return "createResultForClassSeven";
    }

    @RequestMapping("/goToCreateResultClassEight")
    public String goToCreateResultClassEight() {
        return "createResultForClassEight";
    }

    @RequestMapping("/goToCreateResultClassNine")
    public String goToCreateResultClassNine() {
        return "createResultForClassNine";
    }

    @RequestMapping("/goToCreateResultClassTen")
    public String goToCreateResultClassTen() {
        return "createResultForClassTen";
    }
    //End Flow controll for going to CreateResult Page

    //Start of Flow Controll to go to SearchResult
    @RequestMapping("/goToSearchResultClassOne")
    public String goToSearchResultClassOne() {
        return "searchResultForClassOne";
    }

    @RequestMapping("/goToSearchResultClassTwo")
    public String goToSearchResultClassTwo() {
        return "searchResultForClassTwo";
    }

    @RequestMapping("/goToSearchResultClassThree")
    public String goToSearchResultClassThree() {
        return "searchResultForClassThree";
    }

    @RequestMapping("/goToSearchResultClassFour")
    public String goToSearchResultClassFour() {
        return "searchResultForClassFour";
    }

    @RequestMapping("/goToSearchResultClassFive")
    public String goToSearchResultClassFive() {
        return "searchResultForClassFive";
    }

    @RequestMapping("/goToSearchResultClassSix")
    public String goToSearchResultClassSix() {
        return "searchResultForClassSix";
    }

    @RequestMapping("/goToSearchResultClassSeven")
    public String goToSearchResultClassSeven() {
        return "searchResultForClassSeven";
    }

    @RequestMapping("/goToSearchResultClassEight")
    public String goToSearchResultClassEight() {
        return "searchResultForClassEight";
    }

    @RequestMapping("/goToSearchResultClassNine")
    public String goToSearchResultClassNine() {
        return "searchResultForClassNine";
    }

    @RequestMapping("/goToSearchResultClassTen")
    public String goToSearchResultClassTen() {
        return "searchResultForClassTen";
    }
    //Start of Flow Controll to go to SearchResult

    //Flow to Download Admit card Page
    @RequestMapping("/goToDownloadAdmitCardOne")
    public String goToDownloadAdmitCardOne() {
        return "admitCardSearchClassOne";
    }

    @RequestMapping("/goToDownloadAdmitCardTwo")
    public String goToDownloadAdmitCardTwo() {
        return "admitCardSearchClassTwo";
    }

    @RequestMapping("/goToDownloadAdmitCardThree")
    public String goToDownloadAdmitCardThree() {
        return "admitCardSearchClassThree";
    }

    @RequestMapping("/goToDownloadAdmitCardFour")
    public String goToDownloadAdmitCardFour() {
        return "admitCardSearchClassFour";
    }

    @RequestMapping("/goToDownloadAdmitCardFive")
    public String goToDownloadAdmitCardFive() {
        return "admitCardSearchClassFive";
    }

    @RequestMapping("/goToDownloadAdmitCardSix")
    public String goToDownloadAdmitCardSix() {
        return "admitCardSearchClassSix";
    }

    @RequestMapping("/goToDownloadAdmitCardSeven")
    public String goToDownloadAdmitCardSeven() {
        return "admitCardSearchClassSeven";
    }

    @RequestMapping("/goToDownloadAdmitCardEight")
    public String goToDownloadAdmitCardEight() {
        return "admitCardSearchClassEight";
    }

    @RequestMapping("/goToDownloadAdmitCardNine")
    public String goToDownloadAdmitCardNine() {
        return "admitCardSearchClassNine";
    }

    @RequestMapping("/goToSigningUpAdmin")
    public String goToSigningUpAdmin() {
        return "adminSignupForm";
    }

    @RequestMapping("/goToSigningInAdmin")
    public String goToSigningInAdmin() {
        return "adminSignupSuccess";
    }

    @RequestMapping("/goToAddNewTeacher")
    public String goToAddNewTeacher() {
        return "teacherAddForm";
    }

//End of Flow to go to Download Admit card
    //Creating bean
    public ModelLayer findMyBean() {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext();
        context.load("classpath:appContext.xml");
        context.refresh();
        ModelLayer modelBean = context.getBean("modelClass", ModelLayer.class);
        return modelBean;
    }

//End Inserting at  and Querying from All AdmissionTestTables for getting admit cards
    //Start Download Admit Cards
    @RequestMapping("/downloadAdmitCardClassOne")
    public String downloadAdmitCardClassOne(@RequestParam("studentName") String name, @RequestParam("email") String email,
            Model m) {
        ModelLayer modelBean = findMyBean();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        List<ClassOneAdmissionTestTable> list = modelBean.findAdmitCardClassOne(name, email);
        if (list.size() > 0) {
            for (ClassOneAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassOne";
    }

    @RequestMapping("/downloadAdmitCardClassTwo")
    public String downloadAdmitCardClassTwo(@RequestParam("studentName") String name, @RequestParam("email") String email,
            Model m) {
        ModelLayer modelBean = findMyBean();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        List<ClassTwoAdmissionTestTable> list = modelBean.findAdmitCardClassTwo(name, email);
        if (list.size() > 0) {
            for (ClassTwoAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassTwo";
    }

    @RequestMapping("/downloadAdmitCardClassThree")
    public String downloadAdmitCardClassThree(@RequestParam("studentName") String name, @RequestParam("email") String email,
            Model m) {
        ModelLayer modelBean = findMyBean();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        List<ClassThreeAdmissionTestTable> list = modelBean.findAdmitCardClassThree(name, email);
        if (list.size() > 0) {
            for (ClassThreeAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassThree";
    }

    @RequestMapping("/downloadAdmitCardClassFour")
    public String downloadAdmitCardClassFour(@RequestParam("studentName") String name, @RequestParam("email") String email,
            Model m) {
        ModelLayer modelBean = findMyBean();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        List<ClassFourAdmissionTestTable> list = modelBean.findAdmitCardClassFour(name, email);
        if (list.size() > 0) {
            for (ClassFourAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassFour";
    }

    @RequestMapping("/downloadAdmitCardClassFive")
    public String downloadAdmitCardClassFive(@RequestParam("studentName") String name, @RequestParam("email") String email,
            Model m) {
        ModelLayer modelBean = findMyBean();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        List<ClassFiveAdmissionTestTable> list = modelBean.findAdmitCardClassFive(name, email);
        if (list.size() > 0) {
            for (ClassFiveAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassFive";
    }

    @RequestMapping("/downloadAdmitCardClassSix")
    public String downloadAdmitCardClassSix(@RequestParam("studentName") String name, @RequestParam("email") String email,
            Model m) {
        ModelLayer modelBean = findMyBean();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        List<ClassSixAdmissionTestTable> list = modelBean.findAdmitCardClassSix(name, email);
        if (list.size() > 0) {
            for (ClassSixAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassSix";
    }

    @RequestMapping("/downloadAdmitCardClassSeven")
    public String downloadAdmitCardClassSeven(@RequestParam("studentName") String name, @RequestParam("email") String email,
            Model m) {
        ModelLayer modelBean = findMyBean();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        List<ClassSevenAdmissionTestTable> list = modelBean.findAdmitCardClassSeven(name, email);
        if (list.size() > 0) {
            for (ClassSevenAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassSeven";
    }

    @RequestMapping("/downloadAdmitCardClassEight")
    public String downloadAdmitCardClassEight(@RequestParam("studentName") String name, @RequestParam("email") String email,
            Model m) {
        ModelLayer modelBean = findMyBean();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        List<ClassEightAdmissionTestTable> list = modelBean.findAdmitCardClassEight(name, email);
        if (list.size() > 0) {
            for (ClassEightAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassEight";
    }

    @RequestMapping("/downloadAdmitCardClassNine")
    public String downloadAdmitCardClassNine(@RequestParam("studentName") String name, @RequestParam("email") String email,
            Model m) {
        ModelLayer modelBean = findMyBean();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        List<ClassNineAdmissionTestTable> list = modelBean.findAdmitCardClassNine(name, email);
        if (list.size() > 0) {
            for (ClassNineAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
        }
        return "admitCardForClassNine";
    }
    //End of Download Admit card

    //Start of SearchForConfirmAdmission
    @RequestMapping("/searchForConfirmAdmissionClassOne")
    public String searchForConfirmAdmissionClassOne(@RequestParam("serialNumberOneadtest") int serial,
            @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassOneAdmissionTestTable> list = modelBean.confirmAdmissionClassOne(serial, email);
        if (list.size() > 0) {
            for (ClassOneAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
            return "admissionFormClassOne";
        } else {
            m.addAttribute("ErrorMessage", "Please Give Correct Serial Number and Email");
            return "confirmAdmissionForClassOne";
        }

    }

    @RequestMapping("/searchForConfirmAdmissionClassTwo")
    public String searchForConfirmAdmissionClassTwo(@RequestParam("serialNumberTwoadtest") int serial,
            @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassTwoAdmissionTestTable> list = modelBean.confirmAdmissionClassTwo(serial, email);
        if (list.size() > 0) {
            for (ClassTwoAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
            return "admissionFormClassTwo";
        } else {
            m.addAttribute("ErrorMessage", "Please Give Correct Serial Number and Email");
            return "confirmAdmissionForClassTwo";
        }

    }

    @RequestMapping("/searchForConfirmAdmissionClassThree")
    public String searchForConfirmAdmissionClassThree(@RequestParam("serialNumberThreeadtest") int serial,
            @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassThreeAdmissionTestTable> list = modelBean.confirmAdmissionClassThree(serial, email);
        if (list.size() > 0) {
            for (ClassThreeAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
            return "admissionFormClassThree";
        } else {
            m.addAttribute("ErrorMessage", "Please Give Correct Serial Number and Email");
            return "confirmAdmissionForClassThree";
        }

    }

    @RequestMapping("/searchForConfirmAdmissionClassFour")
    public String searchForConfirmAdmissionClassFour(@RequestParam("serialNumberFouradtest") int serial,
            @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassFourAdmissionTestTable> list = modelBean.confirmAdmissionClassFour(serial, email);
        if (list.size() > 0) {
            for (ClassFourAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
            return "admissionFormClassFour";
        } else {
            m.addAttribute("ErrorMessage", "Please Give Correct Serial Number and Email");
            return "confirmAdmissionForClassFour";
        }

    }

    @RequestMapping("/searchForConfirmAdmissionClassFive")
    public String searchForConfirmAdmissionClassFive(@RequestParam("serialNumberFiveadtest") int serial,
            @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassFiveAdmissionTestTable> list = modelBean.confirmAdmissionClassFive(serial, email);
        if (list.size() > 0) {
            for (ClassFiveAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
            return "admissionFormClassFive";
        } else {
            m.addAttribute("ErrorMessage", "Please Give Correct Serial Number and Email");
            return "confirmAdmissionForClassFive";
        }

    }

    @RequestMapping("/searchForConfirmAdmissionClassSix")
    public String searchForConfirmAdmissionClassSix(@RequestParam("serialNumberSixadtest") int serial,
            @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassSixAdmissionTestTable> list = modelBean.confirmAdmissionClassSix(serial, email);
        if (list.size() > 0) {
            for (ClassSixAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
            return "admissionFormClassSix";
        } else {
            m.addAttribute("ErrorMessage", "Please Give Correct Serial Number and Email");
            return "confirmAdmissionForClassSix";
        }

    }

    @RequestMapping("/searchForConfirmAdmissionClassSeven")
    public String searchForConfirmAdmissionClassSeven(@RequestParam("serialNumberSevenadtest") int serial,
            @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassSevenAdmissionTestTable> list = modelBean.confirmAdmissionClassSeven(serial, email);
        if (list.size() > 0) {
            for (ClassSevenAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
            return "admissionFormClassSeven";
        } else {
            m.addAttribute("ErrorMessage", "Please Give Correct Serial Number and Email");
            return "confirmAdmissionForClassSeven";
        }

    }

    @RequestMapping("/searchForConfirmAdmissionClassEight")
    public String searchForConfirmAdmissionClassEight(@RequestParam("serialNumberEightadtest") int serial,
            @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassEightAdmissionTestTable> list = modelBean.confirmAdmissionClassEight(serial, email);
        if (list.size() > 0) {
            for (ClassEightAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
            return "admissionFormClassEight";
        } else {
            m.addAttribute("ErrorMessage", "Please Give Correct Serial Number and Email");
            return "confirmAdmissionForClassEight";
        }

    }

    @RequestMapping("/searchForConfirmAdmissionClassNine")
    public String searchForConfirmAdmissionClassNine(@RequestParam("serialNumberNineadtest") int serial,
            @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        List<ClassNineAdmissionTestTable> list = modelBean.confirmAdmissionClassNine(serial, email);
        if (list.size() > 0) {
            for (ClassNineAdmissionTestTable student : list) {
                m.addAttribute("Student", student);

            }
            return "admissionFormClassNine";
        } else {
            m.addAttribute("ErrorMessage", "Please Give Correct Serial Number and Email");
            return "confirmAdmissionForClassNine";
        }

    }
    //End of SearchForConfirmAdmission

    //Start of Inserting at AdmissionTables and Showing all Informatin for Roll Number.
    @RequestMapping("/admissionForClassOne")
    public String admissionForClassOne(@ModelAttribute("admsnOne") ClassOneAdmissionTable admsnOne,
            @RequestParam("serialNumberOneadtest") int serial, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        admsnOne.setAdmissionDate(sql);
        m.addAttribute("CurrentDate", sql);
        ClassOneAdmissionTestTable adTest = new ClassOneAdmissionTestTable();
        adTest.setSerialNumberOneadtest(serial);
        admsnOne.setClassOneAdmissionTestTable(adTest);
        modelBean.insertClassOneAdmission(admsnOne);
        List<ClassOneAdmissionTable> list = modelBean.findAdmissionInformationForRollNumberClassOne(serial);
        if (list.size() > 0) {
            for (ClassOneAdmissionTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "welcomeAdmissionForClassOne";
        //  return "admissionFormClassOne";
    }

    @RequestMapping("/admissionForClassTwo")
    public String admissionForClassTwo(@ModelAttribute("admsnOne") ClassTwoAdmissionTable admsnTwo,
            @RequestParam("serialNumberTwoadtest") int serial, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        admsnTwo.setAdmissionDate(sql);
        m.addAttribute("CurrentDate", sql);
        ClassTwoAdmissionTestTable adTest = new ClassTwoAdmissionTestTable();
        adTest.setSerialNumberTwoadtest(serial);
        admsnTwo.setClassTwoAdmissionTestTable(adTest);
        modelBean.insertClassTwoAdmission(admsnTwo);
        List<ClassTwoAdmissionTable> list = modelBean.findAdmissionInformationForRollNumberClassTwo(serial);
        if (list.size() > 0) {
            for (ClassTwoAdmissionTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "welcomeAdmissionForClassTwo";
        //return "admissionFormClassTwo";
    }

    @RequestMapping("/admissionForClassThree")
    public String admissionForClassThree(@ModelAttribute("admsnThree") ClassThreeAdmissionTable admsnThree,
            @RequestParam("serialNumberThreeadtest") int serial, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        admsnThree.setAdmissionDate(sql);
        m.addAttribute("CurrentDate", sql);
        ClassThreeAdmissionTestTable adTest = new ClassThreeAdmissionTestTable();
        adTest.setSerialNumberThreeadtest(serial);
        admsnThree.setClassThreeAdmissionTestTable(adTest);
        modelBean.insertClassThreeAdmission(admsnThree);
        List<ClassThreeAdmissionTable> list = modelBean.findAdmissionInformationForRollNumberClassThree(serial);
        if (list.size() > 0) {
            for (ClassThreeAdmissionTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "welcomeAdmissionForClassThree";
        //return "admissionFormClassThree";
    }

    @RequestMapping("/admissionForClassFour")
    public String admissionForClassFour(@ModelAttribute("admsnFour") ClassFourAdmissionTable admsnFour,
            @RequestParam("serialNumberFouradtest") int serial, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        admsnFour.setAdmissionDate(sql);
        m.addAttribute("CurrentDate", sql);
        ClassFourAdmissionTestTable adTest = new ClassFourAdmissionTestTable();
        adTest.setSerialNumberFouradtest(serial);
        admsnFour.setClassFourAdmissionTestTable(adTest);
        modelBean.insertClassFourAdmission(admsnFour);
        List<ClassFourAdmissionTable> list = modelBean.findAdmissionInformationForRollNumberClassFour(serial);
        if (list.size() > 0) {
            for (ClassFourAdmissionTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "welcomeAdmissionForClassFour";
        //return "admissionFormClassFour";
    }

    @RequestMapping("/admissionForClassFive")
    public String admissionForClassFive(@ModelAttribute("admsnFive") ClassFiveAdmissionTable admsnFive,
            @RequestParam("serialNumberFiveadtest") int serial, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        admsnFive.setAdmissionDate(sql);
        m.addAttribute("CurrentDate", sql);
        ClassFiveAdmissionTestTable adTest = new ClassFiveAdmissionTestTable();
        adTest.setSerialNumberFiveadtest(serial);
        admsnFive.setClassFiveAdmissionTestTable(adTest);
        modelBean.insertClassFiveAdmission(admsnFive);
        List<ClassFiveAdmissionTable> list = modelBean.findAdmissionInformationForRollNumberClassFive(serial);
        if (list.size() > 0) {
            for (ClassFiveAdmissionTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "welcomeAdmissionForClassFive";
        //return "admissionFormClassFive";
    }

    @RequestMapping("/admissionForClassSix")
    public String admissionForClassSix(@ModelAttribute("admsnSix") ClassSixAdmissionTable admsnSix,
            @RequestParam("serialNumberSixadtest") int serial, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        admsnSix.setAdmissionDate(sql);
        m.addAttribute("CurrentDate", sql);
        ClassSixAdmissionTestTable adTest = new ClassSixAdmissionTestTable();
        adTest.setSerialNumberSixadtest(serial);
        admsnSix.setClassSixAdmissionTestTable(adTest);
        modelBean.insertClassSixAdmission(admsnSix);
        List<ClassSixAdmissionTable> list = modelBean.findAdmissionInformationForRollNumberClassSix(serial);
        if (list.size() > 0) {
            for (ClassSixAdmissionTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "welcomeAdmissionForClassSix";
        //return "admissionFormClassSix";
    }

    @RequestMapping("/admissionForClassSeven")
    public String admissionForClassSeven(@ModelAttribute("admsnSeven") ClassSevenAdmissionTable admsnSeven,
            @RequestParam("serialNumberSevenadtest") int serial, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        admsnSeven.setAdmissionDate(sql);
        m.addAttribute("CurrentDate", sql);
        ClassSevenAdmissionTestTable adTest = new ClassSevenAdmissionTestTable();
        adTest.setSerialNumberSevenadtest(serial);
        admsnSeven.setClassSevenAdmissionTestTable(adTest);
        modelBean.insertClassSevenAdmission(admsnSeven);
        List<ClassSevenAdmissionTable> list = modelBean.findAdmissionInformationForRollNumberClassSeven(serial);
        if (list.size() > 0) {
            for (ClassSevenAdmissionTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "welcomeAdmissionForClassSeven";
        // return "admissionFormClassSeven";
    }

    @RequestMapping("/admissionForClassEight")
    public String admissionForClassEight(@ModelAttribute("admsnEight") ClassEightAdmissionTable admsnEight,
            @RequestParam("serialNumberEightadtest") int serial, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        admsnEight.setAdmissionDate(sql);
        m.addAttribute("CurrentDate", sql);
        ClassEightAdmissionTestTable adTest = new ClassEightAdmissionTestTable();
        adTest.setSerialNumberEightadtest(serial);
        admsnEight.setClassEightAdmissionTestTable(adTest);
        modelBean.insertClassEightAdmission(admsnEight);
        List<ClassEightAdmissionTable> list = modelBean.findAdmissionInformationForRollNumberClassEight(serial);
        if (list.size() > 0) {
            for (ClassEightAdmissionTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "welcomeAdmissionForClassEight";
        //return "admissionFormClassEight";
    }

    @RequestMapping("/admissionForClassNine")
    public String admissionForClassNine(@ModelAttribute("admsnNine") ClassNineAdmissionTable admsnNine,
            @RequestParam("serialNumberNineadtest") int serial, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        admsnNine.setAdmissionDate(sql);
        m.addAttribute("CurrentDate", sql);
        ClassNineAdmissionTestTable adTest = new ClassNineAdmissionTestTable();
        adTest.setSerialNumberNineadtest(serial);
        admsnNine.setClassNineAdmissionTestTable(adTest);
        modelBean.insertClassNineAdmission(admsnNine);
        List<ClassNineAdmissionTable> list = modelBean.findAdmissionInformationForRollNumberClassNine(serial);
        if (list.size() > 0) {
            for (ClassNineAdmissionTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "welcomeAdmissionForClassNine";
        //return "admissionFormClassNine";
    }
    //End of Inserting at AdmissionTables

    //Start Inserting for Creating Results
    @RequestMapping("/createResultClassOne")
    public String createResultClassOne(@ModelAttribute("result") ClassOneResultTable result,
            @RequestParam("rollNumberOne") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassOneAdmissionTable adTable = new ClassOneAdmissionTable();
        adTable.setRollNumberOne(rollNumber);
        result.setClassOneAdmissionTable(adTable);
        modelBean.insertToCreateResultClassOne(result);
        return "createResultForClassOne";
    }

    @RequestMapping("/createResultClassTwo")
    public String createResultClassTwo(@ModelAttribute("result") ClassTwoResultTable result,
            @RequestParam("rollNumberTwo") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassTwoAdmissionTable adTable = new ClassTwoAdmissionTable();
        adTable.setRollNumberTwo(rollNumber);
        result.setClassTwoAdmissionTable(adTable);
        modelBean.insertToCreateResultClassTwo(result);
        return "createResultForClassTwo";
    }

    @RequestMapping("/createResultClassThree")
    public String createResultClassThree(@ModelAttribute("result") ClassThreeResultTable result,
            @RequestParam("rollNumberThree") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassThreeAdmissionTable adTable = new ClassThreeAdmissionTable();
        adTable.setRollNumberThree(rollNumber);
        result.setClassThreeAdmissionTable(adTable);
        modelBean.insertToCreateResultClassThree(result);
        return "createResultForClassThree";
    }

    @RequestMapping("/createResultClassFour")
    public String createResultClassFour(@ModelAttribute("result") ClassFourResultTable result,
            @RequestParam("rollNumberFour") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassFourAdmissionTable adTable = new ClassFourAdmissionTable();
        adTable.setRollNumberFour(rollNumber);
        result.setClassFourAdmissionTable(adTable);
        modelBean.insertToCreateResultClassFour(result);
        return "createResultForClassFour";
    }

    @RequestMapping("/createResultClassFive")
    public String createResultClassFive(@ModelAttribute("result") ClassFiveResultTable result,
            @RequestParam("rollNumberFive") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassFiveAdmissionTable adTable = new ClassFiveAdmissionTable();
        adTable.setRollNumberFive(rollNumber);
        result.setClassFiveAdmissionTable(adTable);
        modelBean.insertToCreateResultClassFive(result);
        return "createResultForClassFive";
    }

    @RequestMapping("/createResultClassSix")
    public String createResultClassSix(@ModelAttribute("result") ClassSixResultTable result,
            @RequestParam("rollNumberSix") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassSixAdmissionTable adTable = new ClassSixAdmissionTable();
        adTable.setRollNumberSix(rollNumber);
        result.setClassSixAdmissionTable(adTable);
        modelBean.insertToCreateResultClassSix(result);
        return "createResultForClassSix";
    }

    @RequestMapping("/createResultClassSeven")
    public String createResultClassSeven(@ModelAttribute("result") ClassSevenResultTable result,
            @RequestParam("rollNumberSeven") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassSevenAdmissionTable adTable = new ClassSevenAdmissionTable();
        adTable.setRollNumberSeven(rollNumber);
        result.setClassSevenAdmissionTable(adTable);
        modelBean.insertToCreateResultClassSeven(result);
        return "createResultForClassSeven";
    }

    @RequestMapping("/createResultClassEight")
    public String createResultClassEight(@ModelAttribute("result") ClassEightResultTable result,
            @RequestParam("rollNumberEight") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassEightAdmissionTable adTable = new ClassEightAdmissionTable();
        adTable.setRollNumberEight(rollNumber);
        result.setClassEightAdmissionTable(adTable);
        modelBean.insertToCreateResultClassEight(result);
        return "createResultForClassEight";
    }

    @RequestMapping("/createResultClassNine")
    public String createResultClassNine(@ModelAttribute("result") ClassNineResultTable result,
            @RequestParam("rollNumberNine") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassNineAdmissionTable adTable = new ClassNineAdmissionTable();
        adTable.setRollNumberNine(rollNumber);
        result.setClassNineAdmissionTable(adTable);
        modelBean.insertToCreateResultClassNine(result);
        return "createResultForClassNine";
    }

    @RequestMapping("/createResultClassTen")
    public String createResultClassTen(@ModelAttribute("result") ClassTenResultTable result,
            @RequestParam("rollNumberTen") int rollNumber) {
        ModelLayer modelBean = findMyBean();
        ClassTenStudentInformationTable adTable = new ClassTenStudentInformationTable();
        adTable.setRollNumberTen(rollNumber);
        result.setClassTenStudentInformationTable(adTable);
        modelBean.insertToCreateResultClassTen(result);
        return "createResultForClassTen";
    }
    //End of Inserting for Creating Results

    @RequestMapping("/findResultForClassOne")
    public String findResultForClassOne(Model m, @RequestParam("rollNumberOne") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassOneResultTable> list = modelBean.findAllResultClassOne(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassOne";
        } else {
            for (ClassOneResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassOne";
        }
    }

    @RequestMapping("/findResultForClassTwo")
    public String findResultForClassTwo(Model m, @RequestParam("rollNumberTwo") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassTwoResultTable> list = modelBean.findAllResultClassTwo(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassTwo";
        } else {
            for (ClassTwoResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassTwo";
        }
    }

    @RequestMapping("/findResultForClassThree")
    public String findResultForClassThree(Model m, @RequestParam("rollNumberThree") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassThreeResultTable> list = modelBean.findAllResultClassThree(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassThree";
        } else {
            for (ClassThreeResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassThree";
        }
    }

    @RequestMapping("/findResultForClassFour")
    public String findResultForClassFour(Model m, @RequestParam("rollNumberFour") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassFourResultTable> list = modelBean.findAllResultClassFour(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassFour";
        } else {
            for (ClassFourResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassFour";
        }
    }

    @RequestMapping("/findResultForClassFive")
    public String findResultForClassFive(Model m, @RequestParam("rollNumberFive") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassFiveResultTable> list = modelBean.findAllResultClassFive(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassFive";
        } else {
            for (ClassFiveResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassFive";
        }
    }

    @RequestMapping("/findResultForClassSix")
    public String findResultForClassSix(Model m, @RequestParam("rollNumberSix") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassSixResultTable> list = modelBean.findAllResultClassSix(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassSix";
        } else {
            for (ClassSixResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassSix";
        }
    }

    @RequestMapping("/findResultForClassSeven")
    public String findResultForClassSeven(Model m, @RequestParam("rollNumberSeven") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassSevenResultTable> list = modelBean.findAllResultClassSeven(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassSeven";
        } else {
            for (ClassSevenResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassSeven";
        }
    }

    @RequestMapping("/findResultForClassEight")
    public String findResultForClassEight(Model m, @RequestParam("rollNumberEight") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassEightResultTable> list = modelBean.findAllResultClassEight(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassEight";
        } else {
            for (ClassEightResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassEight";
        }
    }

    @RequestMapping("/findResultForClassNine")
    public String findResultForClassNine(Model m, @RequestParam("rollNumberNine") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassNineResultTable> list = modelBean.findAllResultClassNine(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassNine";
        } else {
            for (ClassNineResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassNine";
        }
    }

    @RequestMapping("/findResultForClassTen")
    public String findResultForClassTen(Model m, @RequestParam("rollNumberTen") int roll) {
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        ModelLayer modelBean = findMyBean();
        List<ClassTenResultTable> list = modelBean.findAllResultClassTen(roll);
        if (list.size() == 0) {
            m.addAttribute("ErrorMessage", "Please Enter Correct Roll Number");
            return "searchResultForClassTen";
        } else {
            for (ClassTenResultTable student : list) {
                m.addAttribute("Student", student);
            }
            m.addAttribute("Roll", roll);
            m.addAttribute("CurrentDate", sql);
            return "findResultForClassTen";
        }
    }

//End of Querying for Getting Result
//    @RequestMapping("/addNewTeacher")
//    public String addNewTeacher(Model m, @ModelAttribute("stuff") StuffInformationTable stuff) {
//
//        ModelLayer modelBean = findMyBean();
//        boolean b = modelBean.insertToAddNewTeacher(stuff);
//        if (!b) {
//            m.addAttribute("Message", "Not Succeeded");
//            return "teacherAddForm";
//        } else {
//            m.addAttribute("Message", "Succeeded");
//            return "teacherAddForm";
//        }
//
//    }
    @RequestMapping("/insertToAttendanceTableClassOne")
    public String insertToAttendanceTableClassOne(@RequestParam("rollNumberOne") int roll,
            @ModelAttribute("attOne") ClassOneAttendanceTable attOne, Model m) {
        ModelLayer modelBean = findMyBean();
        ClassOneAdmissionTable ad = new ClassOneAdmissionTable();
        ad.setRollNumberOne(roll);
        attOne.setClassOneAdmissionTable(ad);
        List<ClassOneAdmissionTable> idList = modelBean.findAllIdAttendanceClassOne();
        m.addAttribute("List", idList);
        return "countingAttendanceClassOne";
        //List<ClassOneAttendanceTable> list = new ArrayList<ClassOneAttendanceTable>();
        //list.add(attOne);

        //attOne.setClassOneAdmissionTable(ad);
        //boolean b = modelBean.insertToAttendanceTableClassOne(list);
//        if (b) {
//
//            return "countingAttendanceClassOne";
//        } else {
//            m.addAttribute("ErrorMessage", "Please Insert Correct Email, Password and Admin Code");
//            return "countingAttendanceClassOne";
//        }
    }

    @RequestMapping("/goToCreateAttendanceForClassOne")
    public String goToCreateAttendanceForClassOne(Model m) {
//        ModelLayer modelBean = findMyBean();
//      List<ClassOneAttendanceTable> list =  modelBean.findAttendanceClassOne();
//      m.addAttribute("List", list);
        return "countingAttendanceClassOne";
    }

    //Start Inserting at  and Querying from All AdmissionTestTables for getting admit cards
    @RequestMapping("/insertClassOneAdmissionTest")
    public String insertClassOneAdmissionTest(@ModelAttribute ClassOneAdmissionTestTable oneAdTest,
            @RequestParam("studentName") String name, @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        try {
            Long date = System.currentTimeMillis();
            java.sql.Date sql = new java.sql.Date(date);
            m.addAttribute("Date", sql);
            oneAdTest.setApplyDate(sql);
            oneAdTest.setImage("/images/" + imageNameOne);
            modelBean.insertClassOneAdmissionTest(oneAdTest);
            List<ClassOneAdmissionTestTable> list = modelBean.findAdmitCardClassOne(name, email);
            if (list.size() > 0) {
                for (ClassOneAdmissionTestTable student : list) {
                    m.addAttribute("Student", student);
                }
            }
            return "admitCardForClassOne";
        } catch (Exception e) {
            m.addAttribute("Excp", e.getCause());
            return "admitCardForClassOne";
        }

        //return "classOneAdmissionTestForm";
    }

    @RequestMapping("/insertClassTwoAdmissionTest")
    public String insertClassTwoAdmissionTest(@ModelAttribute("twoAdTest") ClassTwoAdmissionTestTable twoAdTest,
            @RequestParam("studentName") String name, @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        twoAdTest.setApplyDate(sql);
        twoAdTest.setImage("/images/" + imageNameTwo);
        modelBean.insertClassTwoAdmissionTest(twoAdTest);
        List<ClassTwoAdmissionTestTable> list = modelBean.findAdmitCardClassTwo(name, email);
        if (list.size() > 0) {
            for (ClassTwoAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassTwo";
        //return "classTwoAdmissionTestForm";
    }

    @RequestMapping("/insertClassThreeAdmissionTest")
    public String insertClassThreeAdmissionTest(@ModelAttribute("threeAdTest") ClassThreeAdmissionTestTable threeAdTest,
            @RequestParam("studentName") String name, @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        threeAdTest.setApplyDate(sql);
        threeAdTest.setImage("/images/" + imageNameThree);
        modelBean.insertClassThreeAdmissionTest(threeAdTest);
        List<ClassThreeAdmissionTestTable> list = modelBean.findAdmitCardClassThree(name, email);
        if (list.size() > 0) {
            for (ClassThreeAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassThree";
        //return "classThreeAdmissionTestForm";
    }

    @RequestMapping("/insertClassFourAdmissionTest")
    public String insertClassFourAdmissionTest(@ModelAttribute("fourAdTest") ClassFourAdmissionTestTable fourAdTest,
            @RequestParam("studentName") String name, @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        fourAdTest.setApplyDate(sql);
        fourAdTest.setImage("/images/" + imageNameFour);
        modelBean.insertClassFourAdmissionTest(fourAdTest);
        List<ClassFourAdmissionTestTable> list = modelBean.findAdmitCardClassFour(name, email);
        if (list.size() > 0) {
            for (ClassFourAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassFour";
        //return "classFourAdmissionTestForm";
    }

    @RequestMapping("/insertClassFiveAdmissionTest")
    public String insertClassFiveAdmissionTest(@ModelAttribute("fiveAdTest") ClassFiveAdmissionTestTable fiveAdTest,
            @RequestParam("studentName") String name, @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        fiveAdTest.setApplyDate(sql);
        fiveAdTest.setImage("/images/" + imageNameFive);
        modelBean.insertClassFiveAdmissionTest(fiveAdTest);
        List<ClassFiveAdmissionTestTable> list = modelBean.findAdmitCardClassFive(name, email);
        if (list.size() > 0) {
            for (ClassFiveAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassFive";
        //return "classFiveAdmissionTestForm";
    }

    @RequestMapping("/insertClassSixAdmissionTest")
    public String insertClassSixAdmissionTest(@ModelAttribute("sixAdTest") ClassSixAdmissionTestTable sixAdTest,
            @RequestParam("studentName") String name, @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        sixAdTest.setApplyDate(sql);
        sixAdTest.setImage("/images/" + imageNameSix);
        modelBean.insertClassSixAdmissionTest(sixAdTest);
        List<ClassSixAdmissionTestTable> list = modelBean.findAdmitCardClassSix(name, email);
        if (list.size() > 0) {
            for (ClassSixAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassSix";
//        return "classSixAdmissionTestForm";
    }

    @RequestMapping("/insertClassSevenAdmissionTest")
    public String insertClassSevenAdmissionTest(@ModelAttribute("sevenAdTest") ClassSevenAdmissionTestTable sevenAdTest,
            @RequestParam("studentName") String name, @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        sevenAdTest.setApplyDate(sql);
        sevenAdTest.setImage("/images/" + imageNameSeven);
        modelBean.insertClassSevenAdmissionTest(sevenAdTest);
        List<ClassSevenAdmissionTestTable> list = modelBean.findAdmitCardClassSeven(name, email);
        if (list.size() > 0) {
            for (ClassSevenAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassSeven";
        //return "classSevenAdmissionTestForm";
    }

    @RequestMapping("/insertClassEightAdmissionTest")
    public String insertClassEightAdmissionTest(@ModelAttribute("eightAdTest") ClassEightAdmissionTestTable eightAdTest,
            @RequestParam("studentName") String name, @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        eightAdTest.setApplyDate(sql);
        eightAdTest.setImage("/images/" + imageNameEight);
        modelBean.insertClassEightAdmissionTest(eightAdTest);
        List<ClassEightAdmissionTestTable> list = modelBean.findAdmitCardClassEight(name, email);
        if (list.size() > 0) {
            for (ClassEightAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassEight";
        // return "classEightAdmissionTestForm";
    }

    @RequestMapping("/insertClassNineAdmissionTest")
    public String insertClassNineAdmissionTest(@ModelAttribute("nineAdTest") ClassNineAdmissionTestTable nineAdTest,
            @RequestParam("studentName") String name, @RequestParam("email") String email, Model m) {
        ModelLayer modelBean = findMyBean();
        //ModelClass modelBean = new ModelClass();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        m.addAttribute("Date", sql);
        nineAdTest.setApplyDate(sql);
        nineAdTest.setImage("/images/" + imageNameNine);
        modelBean.insertClassNineAdmissionTest(nineAdTest);
        List<ClassNineAdmissionTestTable> list = modelBean.findAdmitCardClassNine(name, email);
        if (list.size() > 0) {
            for (ClassNineAdmissionTestTable student : list) {
                m.addAttribute("Student", student);
            }
        }
        return "admitCardForClassNine";
        //return "classNineAdmissionTestForm";
    }

    String imageNameNine;

    @RequestMapping("/admissionTestPhotoClassNine")
    public String admissionTestPhotoClassNine(HttpServletRequest request, ModelMap m) {

        String path = request.getRealPath("/images");
        path = path.substring(0, path.indexOf("\\build"));
        path = path + "\\web\\images";

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(factory);
        try {
            List<FileItem> itemList = uploader.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField() == false) {
                    item.write(new File(path + "/" + item.getName()));
                    imageNameNine = item.getName();
                    // m.put("ImageURL", "/images/"+imageName);
                }
            }
            ClassNineAdmissionTestTable adtest = new ClassNineAdmissionTestTable();
            adtest.setImage(imageNameNine);
        } catch (Exception e) {
        }
        return "classNineAdmissionTestForm";

    }
    String imageNameEight;

    @RequestMapping("/admissionTestPhotoClassEight")
    public String admissionTestPhotoClassEight(HttpServletRequest request) {

        String path = request.getRealPath("/images");
        path = path.substring(0, path.indexOf("\\build"));
        path = path + "\\web\\images";

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(factory);
        try {
            List<FileItem> itemList = uploader.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField() == false) {
                    item.write(new File(path + "/" + item.getName()));
                    imageNameEight = item.getName();
                }
            }
            ClassEightAdmissionTestTable adtest = new ClassEightAdmissionTestTable();
            adtest.setImage(imageNameEight);
        } catch (Exception e) {
        }
        return "classEightAdmissionTestForm";

    }
    String imageNameSeven;

    @RequestMapping("/admissionTestPhotoClassSeven")
    public String admissionTestPhotoClassSeven(HttpServletRequest request) {

        String path = request.getRealPath("/images");
        path = path.substring(0, path.indexOf("\\build"));
        path = path + "\\web\\images";

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(factory);
        try {
            List<FileItem> itemList = uploader.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField() == false) {
                    item.write(new File(path + "/" + item.getName()));
                    imageNameSeven = item.getName();
                }
            }
            ClassSevenAdmissionTestTable adtest = new ClassSevenAdmissionTestTable();
            adtest.setImage(imageNameSeven);
        } catch (Exception e) {
        }
        return "classSevenAdmissionTestForm";

    }
    String imageNameSix;

    @RequestMapping("/admissionTestPhotoClassSix")
    public String admissionTestPhotoClassSix(HttpServletRequest request) {

        String path = request.getRealPath("/images");
        path = path.substring(0, path.indexOf("\\build"));
        path = path + "\\web\\images";

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(factory);
        try {
            List<FileItem> itemList = uploader.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField() == false) {
                    item.write(new File(path + "/" + item.getName()));
                    imageNameSix = item.getName();
                }
            }
            ClassSixAdmissionTestTable adtest = new ClassSixAdmissionTestTable();
            adtest.setImage(imageNameSix);
        } catch (Exception e) {
        }
        return "classSixAdmissionTestForm";
    }
    String imageNameFive;

    @RequestMapping("/admissionTestPhotoClassFive")
    public String admissionTestPhotoClassFive(HttpServletRequest request, ModelMap m) {

        String path = request.getRealPath("/images");
        path = path.substring(0, path.indexOf("\\build"));
        path = path + "\\web\\images";

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(factory);
        try {
            List<FileItem> itemList = uploader.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField() == false) {
                    item.write(new File(path + "/" + item.getName()));
                    imageNameFive = item.getName();
                }
            }
            ClassFiveAdmissionTestTable adtest = new ClassFiveAdmissionTestTable();
            adtest.setImage(imageNameFive);
            // m.put("ImageURL", "/images/"+imageName);
        } catch (Exception e) {
        }
        return "classFiveAdmissionTestForm";
    }
    String imageNameFour;

    @RequestMapping("/admissionTestPhotoClassFour")
    public String admissionTestPhotoClassFour(HttpServletRequest request) {

        String path = request.getRealPath("/images");
        path = path.substring(0, path.indexOf("\\build"));
        path = path + "\\web\\images";

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(factory);
        try {
            List<FileItem> itemList = uploader.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField() == false) {
                    item.write(new File(path + "/" + item.getName()));
                    imageNameFour = item.getName();
                }
            }
            ClassFourAdmissionTestTable adtest = new ClassFourAdmissionTestTable();
            adtest.setImage(imageNameFour);
        } catch (Exception e) {
        }
        return "classFourAdmissionTestForm";
    }
    String imageNameThree;

    @RequestMapping("/admissionTestPhotoClassThree")
    public String admissionTestPhotoClassThree(HttpServletRequest request) {

        String path = request.getRealPath("/images");
        path = path.substring(0, path.indexOf("\\build"));
        path = path + "\\web\\images";

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(factory);
        try {
            List<FileItem> itemList = uploader.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField() == false) {
                    item.write(new File(path + "/" + item.getName()));
                    imageNameThree = item.getName();
                }
            }
            ClassThreeAdmissionTestTable adtest = new ClassThreeAdmissionTestTable();
            adtest.setImage(imageNameThree);
        } catch (Exception e) {
        }
        return "classThreeAdmissionTestForm";
    }
    String imageNameTwo;

    @RequestMapping("/admissionTestPhotoClassTwo")
    public String admissionTestPhotoClassTwo(HttpServletRequest request) {

        String path = request.getRealPath("/images");
        path = path.substring(0, path.indexOf("\\build"));
        path = path + "\\web\\images";

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(factory);
        try {
            List<FileItem> itemList = uploader.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField() == false) {
                    item.write(new File(path + "/" + item.getName()));
                    imageNameTwo = item.getName();
                }
            }
            ClassTwoAdmissionTestTable adtest = new ClassTwoAdmissionTestTable();
            adtest.setImage(imageNameTwo);
        } catch (Exception e) {
        }
        return "classTwoAdmissionTestForm";
    }
    String imageNameOne;

    @RequestMapping("/admissionTestPhotoClassOne")
    public String admissionTestPhotoClassOne(HttpServletRequest request) {

        String path = request.getRealPath("/images");
        path = path.substring(0, path.indexOf("\\build"));
        path = path + "\\web\\images";

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload uploader = new ServletFileUpload(factory);
        try {
            List<FileItem> itemList = uploader.parseRequest(request);
            for (FileItem item : itemList) {
                if (item.isFormField() == false) {
                    item.write(new File(path + "/" + item.getName()));
                    imageNameOne = item.getName();
                }
            }
            ClassOneAdmissionTestTable adtest = new ClassOneAdmissionTestTable();
            adtest.setImage(imageNameOne);
        } catch (Exception e) {
        }
        return "classOneAdmissionTestForm";
    }
    //End of Uploading File/Image for All Classes

    @RequestMapping("/goToShowStudentInformationClassOne")
    public String goToShowStudentInformationClassOne(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassOneAdmissionTable> list = modelBean.findAllStudentInfoClassOne();
        m.put("List", list);

        return "studentInformaitonClassOne";
    }

    @RequestMapping("/goToShowStudentInformationClassTwo")
    public String goToShowStudentInformationClassTwo(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassTwoAdmissionTable> list = modelBean.findAllStudentInfoClassTwo();
        m.put("List", list);

        return "studentInformaitonClassTwo";
    }

    @RequestMapping("/goToShowStudentInformationClassThree")
    public String goToShowStudentInformationClassThree(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassThreeAdmissionTable> list = modelBean.findAllStudentInfoClassThree();
        m.put("List", list);

        return "studentInformaitonClassThree";
    }

    @RequestMapping("/goToShowStudentInformationClassFour")
    public String goToShowStudentInformationClassFour(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassFourAdmissionTable> list = modelBean.findAllStudentInfoClassFour();
        m.put("List", list);

        return "studentInformaitonClassFour";
    }

    @RequestMapping("/goToShowStudentInformationClassFive")
    public String goToShowStudentInformationClassFive(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassFiveAdmissionTable> list = modelBean.findAllStudentInfoClassFive();
        m.put("List", list);

        return "studentInformaitonClassFive";
    }

    @RequestMapping("/goToShowStudentInformationClassSix")
    public String goToShowStudentInformationClassSix(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassSixAdmissionTable> list = modelBean.findAllStudentInfoClassSix();
        m.put("List", list);

        return "studentInformaitonClassSix";
    }

    @RequestMapping("/goToShowStudentInformationClassSeven")
    public String goToShowStudentInformationClassSeven(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassSevenAdmissionTable> list = modelBean.findAllStudentInfoClassSeven();
        m.put("List", list);

        return "studentInformaitonClassSeven";
    }

    @RequestMapping("/goToShowStudentInformationClassEight")
    public String goToShowStudentInformationClassEight(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassEightAdmissionTable> list = modelBean.findAllStudentInfoClassEight();
        m.put("List", list);

        return "studentInformaitonClassEight";
    }

    @RequestMapping("/goToShowStudentInformationClassNine")
    public String goToShowStudentInformationClassNine(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassNineAdmissionTable> list = modelBean.findAllStudentInfoClassNine();
        m.put("List", list);

        return "studentInformaitonClassNine";
    }

    @RequestMapping("/goToShowStudentInformationClassTen")
    public String goToShowStudentInformationClassTen(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<ClassTenStudentInformationTable> list = modelBean.findAllStudentInfoClassTen();
        m.put("List", list);

        return "studentInformaitonClassTen";
    }
    //End Of Student Information show

    //Start of Get Information from Payment table
    @RequestMapping("/goToPayingFeesClassOne")
    public String goToPayingFeesClassOne(ModelMap m, @ModelAttribute ClassOneAdmissionTable admsn, @RequestParam(value = "rollNumberOne") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassOnePaymentTable> list = modelBean.findOneStudentPayementClassOne(roll);

        for (ClassOnePaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "studentFeesPaymentClassOne";
    }

    @RequestMapping("/goToPayingFeesClassTwo")
    public String goToPayingFeesClassTwo(ModelMap m, @ModelAttribute ClassTwoAdmissionTable admsn, @RequestParam(value = "rollNumberTwo") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassTwoPaymentTable> list = modelBean.findOneStudentPayementClassTwo(roll);

        for (ClassTwoPaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "studentFeesPaymentClassTwo";
    }

    @RequestMapping("/goToPayingFeesClassThree")
    public String goToPayingFeesClassThree(ModelMap m, @ModelAttribute ClassThreeAdmissionTable admsn, @RequestParam(value = "rollNumberThree") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassThreePaymentTable> list = modelBean.findOneStudentPayementClassThree(roll);

        for (ClassThreePaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "studentFeesPaymentClassThree";
    }

    @RequestMapping("/goToPayingFeesClassFour")
    public String goToPayingFeesClassFour(ModelMap m, @ModelAttribute ClassFourAdmissionTable admsn, @RequestParam(value = "rollNumberFour") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassFourPaymentTable> list = modelBean.findOneStudentPayementClassFour(roll);

        for (ClassFourPaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "studentFeesPaymentClassFour";
    }

    @RequestMapping("/goToPayingFeesClassFive")
    public String goToPayingFeesClassFive(ModelMap m, @ModelAttribute ClassFiveAdmissionTable admsn, @RequestParam(value = "rollNumberFive") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassFivePaymentTable> list = modelBean.findOneStudentPayementClassFive(roll);

        for (ClassFivePaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "studentFeesPaymentClassFive";
    }

    @RequestMapping("/goToPayingFeesClassSix")
    public String goToPayingFeesClassSix(ModelMap m, @ModelAttribute ClassSixAdmissionTable admsn, @RequestParam(value = "rollNumberSix") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassSixPaymentTable> list = modelBean.findOneStudentPayementClassSix(roll);

        for (ClassSixPaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "studentFeesPaymentClassSix";
    }

    @RequestMapping("/goToPayingFeesClassSeven")
    public String goToPayingFeesClassSeven(ModelMap m, @ModelAttribute ClassSevenAdmissionTable admsn, @RequestParam(value = "rollNumberSeven") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassSevenPaymentTable> list = modelBean.findOneStudentPayementClassSeven(roll);

        for (ClassSevenPaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "studentFeesPaymentClassSeven";
    }

    @RequestMapping("/goToPayingFeesClassEight")
    public String goToPayingFeesClassEight(ModelMap m, @ModelAttribute ClassEightAdmissionTable admsn, @RequestParam(value = "rollNumberEight") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassEightPaymentTable> list = modelBean.findOneStudentPayementClassEight(roll);

        for (ClassEightPaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "studentFeesPaymentClassEight";
    }

    @RequestMapping("/goToPayingFeesClassNine")
    public String goToPayingFeesClassNine(ModelMap m, @ModelAttribute ClassNineAdmissionTable admsn, @RequestParam(value = "rollNumberNine") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassNinePaymentTable> list = modelBean.findOneStudentPayementClassNine(roll);

        for (ClassNinePaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);
        m.put("Student", admsn);

        return "studentFeesPaymentClassNine";
    }

    @RequestMapping("/goToPayingFeesClassTen")
    public String goToPayingFeesClassTen(ModelMap m, @ModelAttribute ClassTenStudentInformationTable admsn, @RequestParam(value = "rollNumberTen") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassTenPaymentTable> list = modelBean.findOneStudentPayementClassTen(roll);

        for (ClassTenPaymentTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "studentFeesPaymentClassTen";
    }

    //Insert Class One Payment
    @RequestMapping("/classOnePayment")
    public String classOnePayment(ModelMap m, @RequestParam(value = "rollNumberOne") int roll, @ModelAttribute ClassOnePaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassOneAdmissionTable adm = new ClassOneAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberOne(roll);
            payment.setClassOneAdmissionTable(adm);
            modelBean.insertToPaymentTableClassOne(payment);
            m.put("Message", "Insrted Successfully");
            return "studentFeesPaymentClassOne";
        } catch (Exception e) {
            m.put("Message", "Insrt Correct Roll Number");
            return "studentFeesPaymentClassOne";
        }

    }

    @RequestMapping("/classTwoPayment")
    public String classTwoPayment(ModelMap m, @RequestParam(value = "rollNumberTwo") int roll,
            @ModelAttribute ClassTwoPaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassTwoAdmissionTable adm = new ClassTwoAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberTwo(roll);
            payment.setClassTwoAdmissionTable(adm);
            modelBean.insertToPaymentTableClassTwo(payment);
            m.put("Message", "Insrted Successfully");
            return "studentFeesPaymentClassTwo";
        } catch (Exception e) {
            m.put("Message", "Insrt Correct Roll Number");
            return "studentFeesPaymentClassTwo";
        }

    }

    @RequestMapping("/classThreePayment")
    public String classThreePayment(ModelMap m, @RequestParam(value = "rollNumberThree") int roll,
            @ModelAttribute ClassThreePaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassThreeAdmissionTable adm = new ClassThreeAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberThree(roll);
            payment.setClassThreeAdmissionTable(adm);
            modelBean.insertToPaymentTableClassThree(payment);
            m.put("Message", "Insrted Successfully");
            return "studentFeesPaymentClassThree";
        } catch (Exception e) {
            m.put("Message", "Insrt Correct Roll Number");
            return "studentFeesPaymentClassThree";
        }

    }

    @RequestMapping("/classFourPayment")
    public String classFourPayment(ModelMap m, @RequestParam(value = "rollNumberFour") int roll,
            @ModelAttribute ClassFourPaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassFourAdmissionTable adm = new ClassFourAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberFour(roll);
            payment.setClassFourAdmissionTable(adm);
            modelBean.insertToPaymentTableClassFour(payment);
            m.put("Message", "Insrted Successfully");
            return "studentFeesPaymentClassFour";
        } catch (Exception e) {
            m.put("Message", "Insrt Correct Roll Number");
            return "studentFeesPaymentClassFour";
        }

    }

    @RequestMapping("/classFivePayment")
    public String classFivePayment(ModelMap m, @RequestParam(value = "rollNumberFive") int roll,
            @ModelAttribute ClassFivePaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassFiveAdmissionTable adm = new ClassFiveAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberFive(roll);
            payment.setClassFiveAdmissionTable(adm);
            modelBean.insertToPaymentTableClassFive(payment);
            m.put("Message", "Insrted Successfully");
            return "studentFeesPaymentClassFive";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "studentFeesPaymentClassFive";
        }

    }

    @RequestMapping("/classSixPayment")
    public String classSixPayment(ModelMap m, @RequestParam(value = "rollNumberSix") int roll,
            @ModelAttribute ClassSixPaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassSixAdmissionTable adm = new ClassSixAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberSix(roll);
            payment.setClassSixAdmissionTable(adm);
            modelBean.insertToPaymentTableClassSix(payment);
            m.put("Message", "Insrted Successfully");
            return "studentFeesPaymentClassSix";
        } catch (Exception e) {
            m.put("Message", "Insrt Correct Roll Number");
            return "studentFeesPaymentClassSix";
        }

    }

    @RequestMapping("/classSevenPayment")
    public String classSevenPayment(ModelMap m, @RequestParam(value = "rollNumberSeven") int roll,
            @ModelAttribute ClassSevenPaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassSevenAdmissionTable adm = new ClassSevenAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberSeven(roll);
            payment.setClassSevenAdmissionTable(adm);
            modelBean.insertToPaymentTableClassSeven(payment);
            m.put("Message", "Insrted Successfully");
            return "studentFeesPaymentClassSeven";
        } catch (Exception e) {
            m.put("Message", "Insrt Correct Roll Number");
            return "studentFeesPaymentClassSeven";
        }

    }

    @RequestMapping("/classEightPayment")
    public String classEightPayment(ModelMap m, @RequestParam(value = "rollNumberEight") int roll,
            @ModelAttribute ClassEightPaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassEightAdmissionTable adm = new ClassEightAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberEight(roll);
            payment.setClassEightAdmissionTable(adm);
            modelBean.insertToPaymentTableClassEight(payment);
            m.put("Message", "Inserted Successfully");
            return "studentFeesPaymentClassEight";
        } catch (Exception e) {
            m.put("Message", "Insrt Correct Roll Number");
            return "studentFeesPaymentClassEight";
        }

    }

    @RequestMapping("/classNinePayment")
    public String classNinePayment(ModelMap m, @RequestParam(value = "rollNumberNine") int roll,
            @ModelAttribute ClassNinePaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassNineAdmissionTable adm = new ClassNineAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberNine(roll);
            payment.setClassNineAdmissionTable(adm);
            modelBean.insertToPaymentTableClassNine(payment);
            m.put("Message", "Insrted Successfully");
            return "studentFeesPaymentClassNine";
        } catch (Exception e) {
            m.put("Message", "Insrt Correct Roll Number");
            return "studentFeesPaymentClassNine";
        }

    }

    @RequestMapping("/classTenPayment")
    public String classTenPayment(ModelMap m, @RequestParam(value = "rollNumberTen") int roll,
            @ModelAttribute ClassTenPaymentTable payment) {
        ModelLayer modelBean = findMyBean();
        ClassTenStudentInformationTable adm = new ClassTenStudentInformationTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        payment.setDateOfPayment(sql);
        try {
            adm.setRollNumberTen(roll);
            payment.setClassTenStudentInformationTable(adm);
            modelBean.insertToPaymentTableClassTen(payment);
            m.put("Message", "Insrted Successfully");
            return "studentFeesPaymentClassTen";
        } catch (Exception e) {
            m.put("Message", "Insrt Correct Roll Number");
            return "studentFeesPaymentClassTen";
        }

    }

    //Start of Get Information from Attendance table
    @RequestMapping("/goToCreateAttendanceClassOne")
    public String goToCreateAttendanceClassOne(ModelMap m, @ModelAttribute ClassOneAdmissionTable admsn, @RequestParam(value = "rollNumberOne") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassOneAttendanceTable> list = modelBean.findOneStudentAttendanceClassOne(roll);

        for (ClassOneAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassOne";
    }

    @RequestMapping("/goToCreateAttendanceClassTwo")
    public String goToCreateAttendanceClassTwo(ModelMap m, @ModelAttribute ClassTwoAdmissionTable admsn,
            @RequestParam(value = "rollNumberTwo") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassTwoAttendanceTable> list = modelBean.findOneStudentAttendanceClassTwo(roll);

        for (ClassTwoAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassTwo";
    }

    @RequestMapping("/goToCreateAttendanceClassThree")
    public String goToCreateAttendanceClassThree(ModelMap m, @ModelAttribute ClassThreeAdmissionTable admsn, @RequestParam(value = "rollNumberThree") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassThreeAttendanceTable> list = modelBean.findOneStudentAttendanceClassThree(roll);

        for (ClassThreeAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassThree";
    }

    @RequestMapping("/goToCreateAttendanceClassFour")
    public String goToCreateAttendanceClassFour(ModelMap m, @ModelAttribute ClassFourAdmissionTable admsn, @RequestParam(value = "rollNumberFour") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassFourAttendanceTable> list = modelBean.findOneStudentAttendanceClassFour(roll);

        for (ClassFourAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassFour";
    }

    @RequestMapping("/goToCreateAttendanceClassFive")
    public String goToCreateAttendanceClassFive(ModelMap m, @ModelAttribute ClassFiveAdmissionTable admsn, @RequestParam(value = "rollNumberFive") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassFiveAttendanceTable> list = modelBean.findOneStudentAttendanceClassFive(roll);

        for (ClassFiveAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassFive";
    }

    @RequestMapping("/goToCreateAttendanceClassSix")
    public String goToCreateAttendanceClassSix(ModelMap m, @ModelAttribute ClassSixAdmissionTable admsn, @RequestParam(value = "rollNumberSix") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassSixAttendanceTable> list = modelBean.findOneStudentAttendanceClassSix(roll);

        for (ClassSixAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassSix";
    }

    @RequestMapping("/goToCreateAttendanceClassSeven")
    public String goToCreateAttendanceClassSeven(ModelMap m, @ModelAttribute ClassSevenAdmissionTable admsn, @RequestParam(value = "rollNumberSeven") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassSevenAttendanceTable> list = modelBean.findOneStudentAttendanceClassSeven(roll);

        for (ClassSevenAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassSeven";
    }

    @RequestMapping("/goToCreateAttendanceClassEight")
    public String goToCreateAttendanceClassEight(ModelMap m, @ModelAttribute ClassEightAdmissionTable admsn, @RequestParam(value = "rollNumberEight") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassEightAttendanceTable> list = modelBean.findOneStudentAttendanceClassEight(roll);

        for (ClassEightAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassEight";
    }

    @RequestMapping("/goToCreateAttendanceClassNine")
    public String goToCreateAttendanceClassNine(ModelMap m, @ModelAttribute ClassNineAdmissionTable admsn, @RequestParam(value = "rollNumberNine") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassNineAttendanceTable> list = modelBean.findOneStudentAttendanceClassNine(roll);

        for (ClassNineAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassNine";
    }

    @RequestMapping("/goToCreateAttendanceClassTen")
    public String goToCreateAttendanceClassTen(ModelMap m, @ModelAttribute ClassTenStudentInformationTable admsn, @RequestParam(value = "rollNumberTen") int roll) {
        ModelLayer modelBean = findMyBean();
        //ClassOneAdmissionTable adm = new ClassOneAdmissionTable();

        List<ClassTenAttendanceTable> list = modelBean.findOneStudentAttendanceClassTen(roll);

        for (ClassTenAttendanceTable oneStu : list) {
            m.put("List", oneStu);
        }
        m.put("Roll", roll);
        m.put("Student", admsn);

        return "attendanceCreateClassTen";
    }

    //Insert Class One Attendance
    @RequestMapping("/classOneAttendance")
    public String classOneAttendance(ModelMap m, @RequestParam(value = "rollNumberOne") int roll,
            @ModelAttribute ClassOneAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassOneAdmissionTable adm = new ClassOneAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberOne(roll);
            attnd.setClassOneAdmissionTable(adm);
            modelBean.insertToAttendanceTableClassOne(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassOne";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassOne";
        }

    }

    @RequestMapping("/classTwoAttendance")
    public String classTwoAttendance(ModelMap m, @RequestParam(value = "rollNumberTwo") int roll,
            @ModelAttribute ClassTwoAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassTwoAdmissionTable adm = new ClassTwoAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberTwo(roll);
            attnd.setClassTwoAdmissionTable(adm);
            modelBean.insertToAttendanceTableClassTwo(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassTwo";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassTwo";
        }

    }

    @RequestMapping("/classThreeAttendance")
    public String classThreeAttendance(ModelMap m, @RequestParam(value = "rollNumberThree") int roll,
            @ModelAttribute ClassThreeAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassThreeAdmissionTable adm = new ClassThreeAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberThree(roll);
            attnd.setClassThreeAdmissionTable(adm);
            modelBean.insertToAttendanceTableClassThree(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassThree";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassThree";
        }

    }

    @RequestMapping("/classFourAttendance")
    public String classFourAttendance(ModelMap m, @RequestParam(value = "rollNumberFour") int roll,
            @ModelAttribute ClassFourAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassFourAdmissionTable adm = new ClassFourAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberFour(roll);
            attnd.setClassFourAdmissionTable(adm);
            modelBean.insertToAttendanceTableClassFour(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassFour";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassFour";
        }

    }

    @RequestMapping("/classFiveAttendance")
    public String classFiveAttendance(ModelMap m, @RequestParam(value = "rollNumberFive") int roll,
            @ModelAttribute ClassFiveAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassFiveAdmissionTable adm = new ClassFiveAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberFive(roll);
            attnd.setClassFiveAdmissionTable(adm);
            modelBean.insertToAttendanceTableClassFive(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassFive";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassFive";
        }

    }

    @RequestMapping("/classSixAttendance")
    public String classSixAttendance(ModelMap m, @RequestParam(value = "rollNumberSix") int roll,
            @ModelAttribute ClassSixAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassSixAdmissionTable adm = new ClassSixAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberSix(roll);
            attnd.setClassSixAdmissionTable(adm);
            modelBean.insertToAttendanceTableClassSix(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassSix";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassSix";
        }

    }

    @RequestMapping("/classSevenAttendance")
    public String classSevenAttendance(ModelMap m, @RequestParam(value = "rollNumberSeven") int roll,
            @ModelAttribute ClassSevenAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassSevenAdmissionTable adm = new ClassSevenAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberSeven(roll);
            attnd.setClassSevenAdmissionTable(adm);
            modelBean.insertToAttendanceTableClassSeven(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassSeven";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassSeven";
        }

    }

    @RequestMapping("/classEightAttendance")
    public String classEightAttendance(ModelMap m, @RequestParam(value = "rollNumberEight") int roll,
            @ModelAttribute ClassEightAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassEightAdmissionTable adm = new ClassEightAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberEight(roll);
            attnd.setClassEightAdmissionTable(adm);
            modelBean.insertToAttendanceTableClassEight(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassEight";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassEight";
        }

    }

    @RequestMapping("/classNineAttendance")
    public String classNineAttendance(ModelMap m, @RequestParam(value = "rollNumberNine") int roll,
            @ModelAttribute ClassNineAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassNineAdmissionTable adm = new ClassNineAdmissionTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberNine(roll);
            attnd.setClassNineAdmissionTable(adm);
            modelBean.insertToAttendanceTableClassNine(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassNinee";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassNine";
        }

    }

    @RequestMapping("/classTenAttendance")
    public String classTenAttendance(ModelMap m, @RequestParam(value = "rollNumberTen") int roll,
            @ModelAttribute ClassTenAttendanceTable attnd) {
        ModelLayer modelBean = findMyBean();
        ClassTenStudentInformationTable adm = new ClassTenStudentInformationTable();
        Long date = System.currentTimeMillis();
        java.sql.Date sql = new java.sql.Date(date);
        attnd.setDate(sql);
        try {
            adm.setRollNumberTen(roll);
            attnd.setClassTenStudentInformationTable(adm);
            modelBean.insertToAttendanceTableClassTen(attnd);
            m.put("Message", "Inserted Successfully");
            return "attendanceCreateClassTen";
        } catch (Exception e) {
            m.put("Message", "Insert Correct Roll Number");
            return "attendanceCreateClassTen";
        }

    }

    //Edit 
    @RequestMapping("/goToEditClassOne")
    public String goToEditClassOne(ModelMap m, @RequestParam(value = "rollNumberOne") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassOneAdmissionTable> list = modelBean.findOneStudentForEdit(roll);

        for (ClassOneAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassOneInformation";
    }

    @RequestMapping("/goToEditClassTwo")
    public String goToEditClassTwo(ModelMap m, @RequestParam(value = "rollNumberTwo") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassTwoAdmissionTable> list = modelBean.findTwoStudentForEdit(roll);

        for (ClassTwoAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassTwoInformation";
    }

    @RequestMapping("/goToEditClassThree")
    public String goToEditClassThree(ModelMap m, @RequestParam(value = "rollNumberThree") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassThreeAdmissionTable> list = modelBean.findThreeStudentForEdit(roll);

        for (ClassThreeAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassThreeInformation";
    }

    @RequestMapping("/goToEditClassFour")
    public String goToEditClassFour(ModelMap m, @RequestParam(value = "rollNumberFour") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassFourAdmissionTable> list = modelBean.findFourStudentForEdit(roll);

        for (ClassFourAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassFourInformation";
    }

    @RequestMapping("/goToEditClassFive")
    public String goToEditClassFive(ModelMap m, @RequestParam(value = "rollNumberFive") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassFiveAdmissionTable> list = modelBean.findFiveStudentForEdit(roll);

        for (ClassFiveAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassFiveInformation";
    }

    @RequestMapping("/goToEditClassSix")
    public String goToEditClassSix(ModelMap m, @RequestParam(value = "rollNumberSix") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassSixAdmissionTable> list = modelBean.findSixStudentForEdit(roll);

        for (ClassSixAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassSixInformation";
    }

    @RequestMapping("/goToEditClassSeven")
    public String goToEditClassSeven(ModelMap m, @RequestParam(value = "rollNumberSeven") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassSevenAdmissionTable> list = modelBean.findSevenStudentForEdit(roll);

        for (ClassSevenAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassSevenInformation";
    }

    @RequestMapping("/goToEditClassEight")
    public String goToEditClassEight(ModelMap m, @RequestParam(value = "rollNumberEight") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassEightAdmissionTable> list = modelBean.findEightStudentForEdit(roll);

        for (ClassEightAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassEightInformation";
    }

    @RequestMapping("/goToEditClassNine")
    public String goToEditClassNine(ModelMap m, @RequestParam(value = "rollNumberNine") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassNineAdmissionTable> list = modelBean.findNineStudentForEdit(roll);

        for (ClassNineAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassNineInformation";
    }

    @RequestMapping("/goToEditClassTen")
    public String goToEditClassTen(ModelMap m, @RequestParam(value = "rollNumberTen") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassTenStudentInformationTable> list = modelBean.findTenStudentForEdit(roll);

        for (ClassTenStudentInformationTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "editClassTenInformation";
    }

    //Start Edit Information
    @RequestMapping("/editInformationForClassOne")
    public String editInformationForClassOne(ModelMap m, @ModelAttribute ClassOneAdmissionTable addTable,
            @RequestParam("serialNumberOneadtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassOneAdmissionTestTable adtest = new ClassOneAdmissionTestTable();
        adtest.setSerialNumberOneadtest(serial);
        addTable.setClassOneAdmissionTestTable(adtest);
        modelBean.updateInformationClassOne(addTable);
        List<ClassOneAdmissionTable> list = modelBean.findAllStudentInfoClassOne();
        m.put("List", list);

        return "studentInformaitonClassOne";
    }

    @RequestMapping("/editInformationForClassTwo")
    public String editInformationForClassTwo(ModelMap m, @ModelAttribute ClassTwoAdmissionTable addTable,
            @RequestParam("serialNumberTwoadtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassTwoAdmissionTestTable adtest = new ClassTwoAdmissionTestTable();
        adtest.setSerialNumberTwoadtest(serial);
        addTable.setClassTwoAdmissionTestTable(adtest);
        modelBean.updateInformationClassTwo(addTable);
        List<ClassTwoAdmissionTable> list = modelBean.findAllStudentInfoClassTwo();
        m.put("List", list);

        return "studentInformaitonClassTwo";
    }

    @RequestMapping("/editInformationForClassThree")
    public String editInformationForClassThree(ModelMap m, @ModelAttribute ClassThreeAdmissionTable addTable,
            @RequestParam("serialNumberThreeadtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassThreeAdmissionTestTable adtest = new ClassThreeAdmissionTestTable();
        adtest.setSerialNumberThreeadtest(serial);
        addTable.setClassThreeAdmissionTestTable(adtest);
        modelBean.updateInformationClassThree(addTable);
        List<ClassThreeAdmissionTable> list = modelBean.findAllStudentInfoClassThree();
        m.put("List", list);

        return "studentInformaitonClassThree";
    }

    @RequestMapping("/editInformationForClassFour")
    public String editInformationForClassFour(ModelMap m, @ModelAttribute ClassFourAdmissionTable addTable,
            @RequestParam("serialNumberFouradtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassFourAdmissionTestTable adtest = new ClassFourAdmissionTestTable();
        adtest.setSerialNumberFouradtest(serial);
        addTable.setClassFourAdmissionTestTable(adtest);
        modelBean.updateInformationClassFour(addTable);
        List<ClassFourAdmissionTable> list = modelBean.findAllStudentInfoClassFour();
        m.put("List", list);

        return "studentInformaitonClassFour";
    }

    @RequestMapping("/editInformationForClassFive")
    public String editInformationForClassFive(ModelMap m, @ModelAttribute ClassFiveAdmissionTable addTable,
            @RequestParam("serialNumberFiveadtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassFiveAdmissionTestTable adtest = new ClassFiveAdmissionTestTable();
        adtest.setSerialNumberFiveadtest(serial);
        addTable.setClassFiveAdmissionTestTable(adtest);
        modelBean.updateInformationClassFive(addTable);
        List<ClassFiveAdmissionTable> list = modelBean.findAllStudentInfoClassFive();
        m.put("List", list);

        return "studentInformaitonClassFive";
    }

    @RequestMapping("/editInformationForClassSix")
    public String editInformationForClassSix(ModelMap m, @ModelAttribute ClassSixAdmissionTable addTable,
            @RequestParam("serialNumberSixadtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassSixAdmissionTestTable adtest = new ClassSixAdmissionTestTable();
        adtest.setSerialNumberSixadtest(serial);
        addTable.setClassSixAdmissionTestTable(adtest);
        modelBean.updateInformationClassSix(addTable);
        List<ClassSixAdmissionTable> list = modelBean.findAllStudentInfoClassSix();
        m.put("List", list);

        return "studentInformaitonClassSix";
    }

    @RequestMapping("/editInformationForClassSeven")
    public String editInformationForClassSeven(ModelMap m, @ModelAttribute ClassSevenAdmissionTable addTable,
            @RequestParam("serialNumberSevenadtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassSevenAdmissionTestTable adtest = new ClassSevenAdmissionTestTable();
        adtest.setSerialNumberSevenadtest(serial);
        addTable.setClassSevenAdmissionTestTable(adtest);
        modelBean.updateInformationClassSeven(addTable);
        List<ClassSevenAdmissionTable> list = modelBean.findAllStudentInfoClassSeven();
        m.put("List", list);

        return "studentInformaitonClassSeven";
    }

    @RequestMapping("/editInformationForClassEight")
    public String editInformationForClassEight(ModelMap m, @ModelAttribute ClassEightAdmissionTable addTable,
            @RequestParam("serialNumberEightadtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassEightAdmissionTestTable adtest = new ClassEightAdmissionTestTable();
        adtest.setSerialNumberEightadtest(serial);
        addTable.setClassEightAdmissionTestTable(adtest);
        modelBean.updateInformationClassEight(addTable);
        List<ClassEightAdmissionTable> list = modelBean.findAllStudentInfoClassEight();
        m.put("List", list);

        return "studentInformaitonClassEight";
    }

    @RequestMapping("/editInformationForClassNine")
    public String editInformationForClassNine(ModelMap m, @ModelAttribute ClassNineAdmissionTable addTable,
            @RequestParam("serialNumberNineadtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassNineAdmissionTestTable adtest = new ClassNineAdmissionTestTable();
        adtest.setSerialNumberNineadtest(serial);
        addTable.setClassNineAdmissionTestTable(adtest);
        modelBean.updateInformationClassNine(addTable);
        List<ClassNineAdmissionTable> list = modelBean.findAllStudentInfoClassNine();
        m.put("List", list);

        return "studentInformaitonClassNine";
    }

    @RequestMapping("/editInformationForClassTen")
    public String editInformationForClassTen(ModelMap m, @ModelAttribute ClassTenStudentInformationTable addTable) {
        ModelLayer modelBean = findMyBean();
        modelBean.updateInformationClassTen(addTable);
        List<ClassTenStudentInformationTable> list = modelBean.findAllStudentInfoClassTen();
        m.put("List", list);

        return "studentInformaitonClassTen";
    }

    //Go TO search Attendance
    @RequestMapping("/goToShowAttendanceForClassOne")
    public String goToShowAttendanceForClassOne(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusOne";
    }

    @RequestMapping("/goToShowAttendanceForClassTwo")
    public String goToShowAttendanceForClassTwo(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusTwo";
    }

    @RequestMapping("/goToShowAttendanceForClassThree")
    public String goToShowAttendanceForClassThree(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusThree";
    }

    @RequestMapping("/goToShowAttendanceForClassFour")
    public String goToShowAttendanceForClassFour(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusFour";
    }

    @RequestMapping("/goToShowAttendanceForClassFive")
    public String goToShowAttendanceForClassFive(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusFive";
    }

    @RequestMapping("/goToShowAttendanceForClassSix")
    public String goToShowAttendanceForClassSix(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusSix";
    }

    @RequestMapping("/goToShowAttendanceForClassSeven")
    public String goToShowAttendanceForClassSeven(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusSeven";
    }

    @RequestMapping("/goToShowAttendanceForClassEight")
    public String goToShowAttendanceForClassEight(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusEight";
    }

    @RequestMapping("/goToShowAttendanceForClassNine")
    public String goToShowAttendanceForClassNine(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusNine";
    }

    @RequestMapping("/goToShowAttendanceForClassTen")
    public String goToShowAttendanceForClassTen(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchStudentDailyStatusTen";
    }
//End OF Flow to go to searcah Page to show Attendance Table

    //Go TO search Payment Status
    @RequestMapping("/goToSearchFeesPaymentStatusClassOne")
    public String goToSearchFeesPaymentStatusClassOne(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusOne";
    }

    @RequestMapping("/goToSearchFeesPaymentStatusClassTwo")
    public String goToSearchFeesPaymentStatusClassTwo(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusTwo";
    }

    @RequestMapping("/goToSearchFeesPaymentStatusClassThree")
    public String goToSearchFeesPaymentStatusClassThree(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusThree";
    }

    @RequestMapping("/goToSearchFeesPaymentStatusClassFour")
    public String goToSearchFeesPaymentStatusClassFour(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusFour";
    }

    @RequestMapping("/goToSearchFeesPaymentStatusClassFive")
    public String goToSearchFeesPaymentStatusClassFive(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusFive";
    }

    @RequestMapping("/goToSearchFeesPaymentStatusClassSix")
    public String goToSearchFeesPaymentStatusClassSix(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusSix";
    }

    @RequestMapping("/goToSearchFeesPaymentStatusClassSeven")
    public String goToSearchFeesPaymentStatusClassSeven(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusSeven";
    }

    @RequestMapping("/goToSearchFeesPaymentStatusClassEight")
    public String goToSearchFeesPaymentStatusClassEight(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusEight";
    }

    @RequestMapping("/goToSearchFeesPaymentStatusClassNine")
    public String goToSearchFeesPaymentStatusClassNine(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusNine";
    }

    @RequestMapping("/goToSearchFeesPaymentStatusClassTen")
    public String goToSearchFeesPaymentStatusClassTen(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "searchPaymentStatusTen";
    }

    @RequestMapping("/adminHomePage")
    public String adminHomePage(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "indexForAdmin";
    }

    @RequestMapping("/homePage")
    public String homePage(ModelMap m, @ModelAttribute ClassOneAttendanceTable att) {
        return "index";
    }

    //Search Attenndance
    @RequestMapping("/searchStudentStatusClassOne")
    public String searchStudentStatusClassOne(ModelMap m, @RequestParam("rollNumberOne") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassOneAttendanceTable> list = modelBean.findStudentStatusClassOne(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassOne";
    }

    @RequestMapping("/searchStudentStatusClassTwo")
    public String searchStudentStatusClassTwo(ModelMap m, @RequestParam("rollNumberTwo") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassTwoAttendanceTable> list = modelBean.findStudentStatusClassTwo(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassTwo";
    }

    @RequestMapping("/searchStudentStatusClassThree")
    public String searchStudentStatusClassThree(ModelMap m, @RequestParam("rollNumberThree") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassThreeAttendanceTable> list = modelBean.findStudentStatusClassThree(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassThree";
    }

    @RequestMapping("/searchStudentStatusClassFour")
    public String searchStudentStatusClassFour(ModelMap m, @RequestParam("rollNumberFour") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassFourAttendanceTable> list = modelBean.findStudentStatusClassFour(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassFour";
    }

    @RequestMapping("/searchStudentStatusClassFive")
    public String searchStudentStatusClassFive(ModelMap m, @RequestParam("rollNumberFive") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassFiveAttendanceTable> list = modelBean.findStudentStatusClassFive(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassFive";
    }

    @RequestMapping("/searchStudentStatusClassSix")
    public String searchStudentStatusClassSix(ModelMap m, @RequestParam("rollNumberSix") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassSixAttendanceTable> list = modelBean.findStudentStatusClassSix(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassSix";
    }

    @RequestMapping("/searchStudentStatusClassSeven")
    public String searchStudentStatusClassSeven(ModelMap m, @RequestParam("rollNumberSeven") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassSevenAttendanceTable> list = modelBean.findStudentStatusClassSeven(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassSeven";
    }

    @RequestMapping("/searchStudentStatusClassEight")
    public String searchStudentStatusClassEight(ModelMap m, @RequestParam("rollNumberEight") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassEightAttendanceTable> list = modelBean.findStudentStatusClassEight(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassEight";
    }

    @RequestMapping("/searchStudentStatusClassNine")
    public String searchStudentStatusClassNine(ModelMap m, @RequestParam("rollNumberNine") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassNineAttendanceTable> list = modelBean.findStudentStatusClassNine(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassNine";
    }

    @RequestMapping("/searchStudentStatusClassTen")
    public String searchStudentStatusClassTen(ModelMap m, @RequestParam("rollNumberTen") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassTenAttendanceTable> list = modelBean.findStudentStatusClassTen(roll);
        m.put("List", list);
        return "showStudentsDailyStatusClassTen";
    }
    //End Of Getting Status Information

    //Delete From Admission Table
    @RequestMapping("/goDeleteClassOne")
    public String goDeleteClassOne(ModelMap m, @RequestParam(value = "rollNumberOne") int roll) {
        ModelLayer modelBean = findMyBean();

        List<ClassOneAdmissionTable> list = modelBean.findOneStudentForEdit(roll);

        for (ClassOneAdmissionTable oneStu : list) {
            if (oneStu.getGender().equalsIgnoreCase("Male")) {
                oneStu.setGender("Male");
            } else if (oneStu.getGender().equalsIgnoreCase("Female")) {
                oneStu.setGender("Female");
            } else {
                oneStu.setGender("Third");
            }
            m.put("Student", oneStu);
        }
        //m.put("Roll", roll);

        return "showInformationForDeleteClassOne";
    }

    @RequestMapping("/deleteInformationClassOne")
    public String deleteInformationClassOne(ModelMap m, @ModelAttribute ClassOneAdmissionTable addTable,
            @RequestParam("serialNumberOneadtest") int serial) {
        ModelLayer modelBean = findMyBean();
        ClassOneAdmissionTestTable test = new ClassOneAdmissionTestTable();
        test.setSerialNumberOneadtest(serial);
        addTable.setClassOneAdmissionTestTable(test);
        modelBean.deleteStudentClassOne(addTable);
        List<ClassOneAdmissionTable> list = modelBean.findAllStudentInfoClassOne();
        m.put("List", list);

        return "studentInformaitonClassOne";
    }

    //Search Payment status
    @RequestMapping("/searchPaymentStatusClassOne")
    public String searchPaymentStatusClassOne(ModelMap m, @RequestParam("rollNumberOne") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassOnePaymentTable> list = modelBean.findPaymentStatusClassOne(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassOne";
    }

    @RequestMapping("/searchPaymentStatusClassTwo")
    public String searchPaymentStatusClassTwo(ModelMap m, @RequestParam("rollNumberTwo") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassTwoPaymentTable> list = modelBean.findPaymentStatusClassTwo(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassTwo";
    }

    @RequestMapping("/searchPaymentStatusClassThree")
    public String searchPaymentStatusClassThree(ModelMap m, @RequestParam("rollNumberThree") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassThreePaymentTable> list = modelBean.findPaymentStatusClassThree(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassThree";
    }

    @RequestMapping("/searchPaymentStatusClassFour")
    public String searchPaymentStatusClassFour(ModelMap m, @RequestParam("rollNumberFour") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassFourPaymentTable> list = modelBean.findPaymentStatusClassFour(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassFour";
    }

    @RequestMapping("/searchPaymentStatusClassFive")
    public String searchPaymentStatusClassFive(ModelMap m, @RequestParam("rollNumberFive") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassFivePaymentTable> list = modelBean.findPaymentStatusClassFive(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassFive";
    }

    @RequestMapping("/searchPaymentStatusClassSix")
    public String searchPaymentStatusClassSix(ModelMap m, @RequestParam("rollNumberSix") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassSixPaymentTable> list = modelBean.findPaymentStatusClassSix(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassSix";
    }

    @RequestMapping("/searchPaymentStatusClassSeven")
    public String searchPaymentStatusClassSeven(ModelMap m, @RequestParam("rollNumberSeven") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassSevenPaymentTable> list = modelBean.findPaymentStatusClassSeven(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassSeven";
    }

    @RequestMapping("/searchPaymentStatusClassEight")
    public String searchPaymentStatusClassEight(ModelMap m, @RequestParam("rollNumberEight") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassEightPaymentTable> list = modelBean.findPaymentStatusClassEight(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassEight";
    }

    @RequestMapping("/searchPaymentStatusClassNine")
    public String searchPaymentStatusClassNine(ModelMap m, @RequestParam("rollNumberNine") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassNinePaymentTable> list = modelBean.findPaymentStatusClassNine(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassNine";
    }

    @RequestMapping("/searchPaymentStatusClassTen")
    public String searchPaymentStatusClassTen(ModelMap m, @RequestParam("rollNumberTen") int roll) {
        ModelLayer modelBean = findMyBean();
        List<ClassTenPaymentTable> list = modelBean.findPaymentStatusClassTen(roll);
        m.put("List", list);
        return "showInformationPaymentStautsClassTen";
    }

    @RequestMapping("/goToDownloadStudentNotice")
    public String goToDownloadStudentNotice(ModelMap m) {

        return "studentNoticeDownload";
    }
//    private static final String INTERNAL_FILE = "/images/School Management System.pdf";
//
//    @RequestMapping(value = "/download/{type}", method = RequestMethod.GET)
//    public void downloadFile(HttpServletResponse response, @PathVariable("type") String type) throws IOException {
//        File file = null;
//        if (type.equalsIgnoreCase("internal")) {
//            ClassLoader loader = Thread.currentThread().getContextClassLoader();
//            file = new File(loader.getResource(INTERNAL_FILE).getFile());
//        }
//        if (!file.exists()) {
//            String errorMessage = "Sorry. The file you are looking for does not exist";
//            //System.out.println(errorMessage);
//            OutputStream outputStream = response.getOutputStream();
//            outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
//            outputStream.close();
//            return;
//        }
//        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
//        if (mimeType == null) {
//            System.out.println("mimetype is not detectable, will take default");
//            mimeType = "application/pdf";
//        }
//        System.out.println("mimetype : " + mimeType);
//
//        response.setContentType(mimeType);
//        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));
//        response.setContentLength((int) file.length());
//        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
//        FileCopyUtils.copy(inputStream, response.getOutputStream());
//    }

//    @RequestMapping("/downloadStudentNotce")
//    public void downloadStudentNotce(ModelMap m,HttpSession session,HttpServletResponse response) {
//       try {
//        String filePathToBeServed = "/images/Booting Help.txt";//complete file name with path;
//        File fileToDownload = new File(filePathToBeServed);
//           InputStream inputStream = new FileInputStream(fileToDownload);
//           String fileName = "E:\\Back UP\\givenName";
//        response.setContentType("application/force-download");
//        response.setHeader("Content-Disposition", "attachment; filename="+fileName+".txt"); 
//           IOUtils.copy(inputStream, response.getOutputStream());
//        response.flushBuffer();
//        inputStream.close();
//    } catch (Exception e){
//        //LOGGER.debug("Request could not be completed at this moment. Please try again.");
//        e.printStackTrace();
//    }
//        //return "studentNoticeDownload";
//    }
    @RequestMapping("/showAllTeacherInformation")
    public String showAllTeacherInformation(ModelMap m) {
        ModelLayer modelBean = findMyBean();
        List<StuffInformationTable> list = modelBean.findAllTeachers();
        m.put("List", list);
        return "showAllTeachersInformtion";
    }

    @RequestMapping("/adminSignupPage")
    public String adminSignupPage(ModelMap m) {
//        ModelLayer modelBean = findMyBean();
//        List<StuffInformationTable> list = modelBean.findAllTeachers();
//        m.put("List", list);
        return "adminSignupForm";
    }

//    @RequestMapping("/registerAdmin")
//    public void registerAdmin(@ModelAttribute AdminLoginTable addmnTable) {
//        ModelLayer modelBean = findMyBean();
//        
//        modelBean.insertForRegisterAdmin(addmnTable);
//        //return "showAllTeachersInformtion";
//    }
    @RequestMapping("/registerAdmin")
    public String registerAdmin(@RequestParam("admincode") String code, @ModelAttribute("adminTable") AdminLoginTable adminTable,
            Model m) {
        String admin = "admin123456";
        if (!code.equals(admin)) {
            m.addAttribute("ErrorMessage", "Registration Fail!! Please Insert Correct Admin Code");
            return "index";
        } else {
            ModelLayer modelBean = findMyBean();
            modelBean.createSignup(adminTable);
            m.addAttribute("SuccessMessage", "Registration Successful!! Please Login");
            return "index";
        }
    }

    @RequestMapping("/signupForAdmin")
    public String signupForAdmin(@RequestParam("admincode") String code, @ModelAttribute("adminTable") AdminLoginTable adminTable,
            Model m) {
        String admin = "admin123456";
        if (!code.equals(admin)) {
            m.addAttribute("ErrorMessage", "Please Insert Correct Admin Code");
            return "adminSignupForm";
        } else {
            ModelLayer modelBean = findMyBean();
            modelBean.createSignup(adminTable);
            return "adminSignupSuccess";
        }
    }

    @RequestMapping("/adminLogin")
    public String adminLogin(@RequestParam("email") String email, @RequestParam("password") String password,
            @RequestParam("admincode") String code, Model m) {
        ModelLayer modelBean = findMyBean();
        boolean b = modelBean.adminLoginConfirm(email, password, code);

        if (b) {

            return "indexForAdmin";
        } else {
            m.addAttribute("ErrorMessage", "Please Insert Correct Email, Password and Admin Code");
            return "adminSignupSuccess";
        }
    }
    
    @RequestMapping("/loginAdminPopup")
    public String loginAdminPopup(@RequestParam("email") String email, @RequestParam("password") String password,
            @RequestParam("admincode") String code, Model m) {
        ModelLayer modelBean = findMyBean();
        boolean b = modelBean.adminLoginConfirm(email, password, code);

        if (b) {

            return "indexForAdmin";
        } else {
            m.addAttribute("ErrorMessage", "Please Insert Correct Email, Password and Admin Code");
            return "index";
        }
    }

}
